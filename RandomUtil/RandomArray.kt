package RandomUtil

fun <T> Array<T>.shuffle() { for (item in this.asList().shuffled().withIndex()) this[item.index] = item.value }
fun <T: Any> randomArray(length: Int, generator: (Int) -> T): Array<T> = Array<Any>(length) { generator(it) } as Array<T>