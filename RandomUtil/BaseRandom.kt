import kotlin.random.*

/**
 * @author: nbness2
 */

/**
 * [BaseRandom] is basically a wrapper for [kotlin.random.Random], nothing quite special about it.
 */
abstract class BaseRandom<T>(seed: Long?=null) {
    private val internalRandom: Random = Random(seed ?: System.currentTimeMillis())
    internal fun nextByte(lowerBound: Byte, upperBound: Byte, inclusive: Boolean): Byte = internalRandom.nextInt(lowerBound.toInt(), upperBound + (if (inclusive) 1 else 0)).toByte()
    internal fun nextShort(lowerBound: Short, upperBound: Short, inclusive: Boolean): Short = internalRandom.nextInt(lowerBound.toInt(), upperBound + (if (inclusive) 1 else 0)).toShort()
    internal fun nextInt(lowerBound: Int, upperBound: Int, inclusive: Boolean): Int = internalRandom.nextLong(lowerBound.toLong(), upperBound + (if (inclusive) 1 else 0).toLong()).toInt()
    internal fun nextLong(lowerBound: Long, upperBound: Long, inclusive: Boolean): Long = internalRandom.nextLong(lowerBound, upperBound + (if (inclusive) 1 else 0))
    internal fun nextFloat(lowerBound: Float, upperBound: Float): Float = internalRandom.nextDouble(lowerBound.toDouble(), upperBound.toDouble()).toFloat()
    internal fun nextDouble(lowerBound: Double, upperBound: Double): Double = internalRandom.nextDouble(lowerBound, upperBound)
    abstract fun pickItem(): T
    abstract fun copyOf(): BaseRandom<T>
}