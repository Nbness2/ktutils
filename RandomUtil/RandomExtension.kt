package RandomUtil

import kotlin.random.Random

/**
 * @author: nbness2
 */

/**
 * The following extensions give a random value within a range (except boolean).
 */
fun Boolean.Companion.random() = Random.nextBoolean()
fun Byte.Companion.random(lowerBound: Byte=Byte.MIN_VALUE, upperBound: Byte=Byte.MAX_VALUE, inclusive: Boolean=false): Byte = Random.nextInt(lowerBound.toInt(), upperBound.toInt() + if (inclusive) 1 else 0).toByte()
fun Short.Companion.random(lowerBound: Short=Short.MIN_VALUE, upperBound: Short=Short.MAX_VALUE, inclusive: Boolean=false): Short = Random.nextInt(lowerBound.toInt(), upperBound.toInt() + if (inclusive) 1 else 0).toShort()
fun Int.Companion.random(lowerBound: Int=Int.MIN_VALUE, upperBound: Int=Int.MAX_VALUE, inclusive: Boolean=false): Int = Random.nextLong(lowerBound.toLong(), upperBound.toLong() + if (inclusive) 1 else 0).toInt()
fun Long.Companion.random(lowerBound: Long=Long.MIN_VALUE, upperBound: Long=Long.MAX_VALUE, inclusive: Boolean=false): Long = Random.nextLong(lowerBound, upperBound + if (inclusive) 1 else 0)
fun Float.Companion.random(lowerBound: Float=Float.MIN_VALUE, upperBound: Float=Float.MAX_VALUE): Float = Random.nextDouble(lowerBound.toDouble(), upperBound.toDouble()).toFloat()
fun Double.Companion.random(lowerBound: Double=Double.MIN_VALUE, upperBound: Double=Double.MAX_VALUE): Double = Random.nextDouble(lowerBound, upperBound)

fun Byte.random(): Byte = Byte.random(lowerBound=minOf(this, 0), upperBound = maxOf(this, 0), inclusive=false)


/**
 * [String.random] generates a random string based on a set of its own contents.
 */
fun String.random(length: Int): String {
    val stringSet = this.toSet()
    val randomizedString = StringBuilder()
    repeat(length) { randomizedString.append(stringSet.random()) }
    return randomizedString.toString()
}

/**
 * Shuffles the [Array] in place. IDK a cleaner way to do it so... yeet.
 */
fun <T> Array<T>.shuffle() { for (item in this.asList().shuffled().withIndex()) this[item.index] = item.value }
fun ByteArray.shuffle() { for (item in this.asList().shuffled().withIndex()) this[item.index] = item.value }
fun ShortArray.shuffle() { for (item in this.asList().shuffled().withIndex()) this[item.index] = item.value }
fun IntArray.shuffle() { for (item in this.asList().shuffled().withIndex()) this[item.index] = item.value }
fun LongArray.shuffle() { for (item in this.asList().shuffled().withIndex()) this[item.index] = item.value }
fun FloatArray.shuffle() { for (item in this.asList().shuffled().withIndex()) this[item.index] = item.value }
fun DoubleArray.shuffle() { for (item in this.asList().shuffled().withIndex()) this[item.index] = item.value }
fun CharArray.shuffle() { for (item in this.asList().shuffled().withIndex()) this[item.index] = item.value }

/**
 * Returns [this] but shuffled around.
 */
fun String.shuffled(): String { return this.toCharArray().toMutableList().shuffled().joinToString("") }


fun main() {
    val a = intArrayOf(1, 2, 3, 4, 5, 6, 7, 8, 9, 10).toTypedArray()
    a.shuffle()
    println(a.contentToString())
}

