package RandomUtil

import kotlin.random.Random

/**
 * @author: nbness2
 */

/**
 * The following extensions give a random value within a range (except boolean).
 */
fun Boolean.Companion.random() = Random.nextBoolean()
fun Byte.Companion.random(lowerBound: Byte=Byte.MIN_VALUE, upperBound: Byte=Byte.MAX_VALUE, inclusive: Boolean=false): Byte = Random.nextInt(lowerBound.toInt(), upperBound.toInt() + if (inclusive) 1 else 0).toByte()
fun Short.Companion.random(lowerBound: Short=Short.MIN_VALUE, upperBound: Short=Short.MAX_VALUE, inclusive: Boolean=false): Short = Random.nextInt(lowerBound.toInt(), upperBound.toInt() + if (inclusive) 1 else 0).toShort()
fun Int.Companion.random(lowerBound: Int=Int.MIN_VALUE, upperBound: Int=Int.MAX_VALUE, inclusive: Boolean=false): Int = Random.nextLong(lowerBound.toLong(), upperBound.toLong() + if (inclusive) 1 else 0).toInt()
fun Long.Companion.random(lowerBound: Long=Long.MIN_VALUE, upperBound: Long=Long.MAX_VALUE, inclusive: Boolean=false): Long = Random.nextLong(lowerBound, upperBound + if (inclusive) 1 else 0)
fun Float.Companion.random(lowerBound: Float=Float.MIN_VALUE, upperBound: Float=Float.MAX_VALUE): Float = Random.nextDouble(lowerBound.toDouble(), upperBound.toDouble()).toFloat()
fun Double.Companion.random(lowerBound: Double=Double.MIN_VALUE, upperBound: Double=Double.MAX_VALUE): Double = Random.nextDouble(lowerBound, upperBound)

fun Byte.random(): Byte = Byte.random(lowerBound=minOf(this, 0), upperBound = maxOf(this, 0), inclusive=false)
fun Short.random(): Short = Short.random(lowerBound=minOf(this, 0), upperBound = maxOf(this, 0), inclusive=false)
fun Int.random(): Int = Int.random(lowerBound=minOf(this, 0), upperBound = maxOf(this, 0), inclusive=false)
fun Long.random(): Long = Long.random(lowerBound=minOf(this, 0), upperBound = maxOf(this, 0), inclusive=false)
fun Float.random(): Float = Float.random(lowerBound=minOf(this, 0.0f), upperBound = maxOf(this, 0.0f))
fun Double.random(): Double = Double.random(lowerBound=minOf(this, 0.0), upperBound = maxOf(this, 0.0))

/**
 * Generates a random primitive [Array] of length [length] within the given parameters
 */
fun randomBooleanArray(length: Int): BooleanArray = BooleanArray(length) { Boolean.random() }
fun randomByteArray(length: Int, lowerBound: Byte=Byte.MIN_VALUE, upperBound: Byte=Byte.MAX_VALUE, inclusive: Boolean=false): ByteArray = ByteArray(length) { Byte.random(lowerBound=lowerBound, upperBound=upperBound, inclusive=inclusive) }
fun randomShortArray(length: Int, lowerBound: Short=Short.MIN_VALUE, upperBound: Short=Short.MAX_VALUE, inclusive: Boolean=false): ShortArray = ShortArray(length) { Short.random(lowerBound=lowerBound, upperBound=upperBound, inclusive=inclusive) }
fun randomIntArray(length: Int, lowerBound: Int=Int.MIN_VALUE, upperBound: Int=Int.MAX_VALUE, inclusive: Boolean=false): IntArray = IntArray(length) { Int.random(lowerBound=lowerBound, upperBound=upperBound, inclusive=inclusive) }
fun randomLongArray(length: Int, lowerBound: Long=Long.MIN_VALUE, upperBound: Long=Long.MAX_VALUE, inclusive: Boolean=false): LongArray = LongArray(length) { Long.random(lowerBound=lowerBound, upperBound=upperBound, inclusive=inclusive) }
fun randomFloatArray(length: Int, lowerBound: Float=Float.MIN_VALUE, upperBound: Float=Float.MAX_VALUE): FloatArray = FloatArray(length) { Float.random(lowerBound=lowerBound, upperBound=upperBound) }
fun randomDoubleArray(length: Int, lowerBound: Double=Double.MIN_VALUE, upperBound: Double=Double.MAX_VALUE): DoubleArray = DoubleArray(length) { Double.random(lowerBound=lowerBound, upperBound=upperBound) }


/**
 * Shuffles the [Array] in place. IDK a cleaner way to do it so... yeet.
 */
fun ByteArray.shuffle() { for (item in this.asList().shuffled().withIndex()) this[item.index] = item.value }
fun ShortArray.shuffle() { for (item in this.asList().shuffled().withIndex()) this[item.index] = item.value }
fun IntArray.shuffle() { for (item in this.asList().shuffled().withIndex()) this[item.index] = item.value }
fun LongArray.shuffle() { for (item in this.asList().shuffled().withIndex()) this[item.index] = item.value }
fun FloatArray.shuffle() { for (item in this.asList().shuffled().withIndex()) this[item.index] = item.value }
fun DoubleArray.shuffle() { for (item in this.asList().shuffled().withIndex()) this[item.index] = item.value }
fun CharArray.shuffle() { for (item in this.asList().shuffled().withIndex()) this[item.index] = item.value }


fun main() {
    val a = arrayOf(1, 2, 3, 4, 5, 6, 7, "stwing", 9, 10)
    a.shuffle()
    println(a.contentToString())
}

