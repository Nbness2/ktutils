/**
 * @author: nbness2
 */

/**
 * [RandomTable] is a table that is picked from at random.
 * There is no inherent bias past the PRNG bias after a lot of cycles, as there arent any weights assigned to each item.
 */
open class RandomTable <T>: BaseRandom<T> {
    private val items: Array<T>
    internal val itemList: List<T>

    constructor(items: Array<T>) {
        this.items = items.copyOf()
        this.itemList = items.asList()
    }

    /**
     * This get operator uses python indexing
     * Python indexing means that -1 is last item of iterable, -2 is second to last, -size is first, out of bounds otherwise.
     * This is a bit more clear (iterable[-whatever]) than the alternative (iterable[size-whatever])
     */
    operator fun get(index: Int): T {
        var finalIndex = index
        if (finalIndex > this.items.size-1 || finalIndex < -this.items.size)
            throw ArrayIndexOutOfBoundsException("Index ($finalIndex) out of range for length: ${this.items.size}")
        finalIndex += if (finalIndex < 0) this.items.size else 0
        return this.items[finalIndex]
    }

    /**
     * Checks if [item] is contained in [items]
     */
    operator fun contains(item: T): Boolean = item in this.items

    /**
     * Gets the index of the given [item]
     */
    fun getIndexOf(item: T): Int {
        if (item !in this.items)
            throw ArrayStoreException("Item $item not contained.")
        return this.items.indexOf(item)
    }

    /**
     * This sorts [items] according to [other].
     * The item in [other] corresponds to the item with the same index in [items].
     */
    protected fun sortItemsBy(other: IntArray) {
        fun IntArray.sortedByOther(other: IntArray): Pair<IntArray, IntArray> {
            if (this.size != other.size)
                throw ArrayStoreException("sortedByOther: this and other must be same size")
            val sortedArray: Array<Pair<Int, Int>> =
                Array(this.size) { idx -> this[idx] to other[idx] }
            sortedArray.sortBy { it.second }
            return sortedArray.map { it.first }.toIntArray() to sortedArray.map{ it.second }.toIntArray()
        }
        val prev = this.items.clone()
        val indices = IntArray(other.size) { it }

        for ((newIndex, oldIndex) in indices.sortedByOther(other).first.withIndex())
            this.items[newIndex] = prev[oldIndex]
        other.sort()
    }

    /**
     * This is the internal pickItem function that the other pickItem functions use.
     * Modifier is a parameter that can be used for implementations of [RandomTable].
     */
    @JvmOverloads
    internal open fun pickInternal(modifier: Int=0): T = this.items[this.nextInt(0, this.items.size, false)]

    /**
     * Picks a single item
     */
    override fun pickItem(): T = this.pickInternal(0)
    fun pickItem(modifier: Int): T = this.pickInternal(modifier=modifier)

    /**
     * Picks [pickAmount] items, puts them in a map and assigns them a counter to keep track of how many times they got picked.
     * Good if you pick a lot of items and dont care about order.
     */
    @JvmOverloads
    fun pickMap(pickAmount: Int, modifier: Int=0): Map<T, Int> {
        val resultsMap = HashMap<T, Int>()
        var result: T
        var previousCount: Int
        for (run in 0 until pickAmount) {
            result = this.pickInternal(modifier=modifier)
            resultsMap.putIfAbsent(result, 0)
            previousCount = resultsMap[result] as Int
           resultsMap[result] = previousCount + 1
        }
        return resultsMap
    }

    /**
     * Picks [pickAmount] items and puts them in a [List]. Good for order-critical picks.
     */
    @JvmOverloads
    fun pickOrdered(pickAmount: Int, modifier: Int=0): List<T> {
        val resultsList = ArrayList<T>(pickAmount)
        for (listIndex in 0 until pickAmount)
            resultsList[listIndex] = this.pickInternal(modifier=modifier)
        return resultsList
    }

    override fun copyOf(): RandomTable<T> = RandomTable(this.items.copyOf())
}
