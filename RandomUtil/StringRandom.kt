package RandomUtil

/**
 * [String.setRandom] generates a random string based on a set of its own contents of a [length] length.
 */
fun String.setRandom(length: Int): String {
    val stringSet = this.toSet()
    val randomizedString = StringBuilder()
    repeat(length) { randomizedString.append(stringSet.random()) }
    return randomizedString.toString()
}

fun String.random(length: Int): String {
    val randomizedString = StringBuilder()
    repeat(length) { randomizedString.append(this.random()) }
    return randomizedString.toString()
}

/**
 * Returns [this] but shuffled around.
 */
fun String.shuffled(): String { return this.toCharArray().toMutableList().shuffled().joinToString("") }

/**
 * Creates a random [Array] (length [arrayLength]) of [String] (length [stringLength]).
 * Strings are generated from [sourceString].
 */
fun randomStringArray(arrayLength: Int, stringLength: Int, sourceString: String): Array<String> = Array(arrayLength) { sourceString.random(stringLength) }


fun main() {
}