/**
 * @author: nbness2
 */

/**
 * [MemoWeightedTable] is essentially a [WeightedTable] that can use custom weights OR custom items (not both).
 * Each [MemoWeightedTable] instance remembers what custom weight and custom item arrays you put in using [hashCode].
 * They also remember whether or not the given arrays were valid or not.
 */
open class MemoWeightedTable<T>(items: Array<T>, private val initialWeights: IntArray) : WeightedTable<T>(items, initialWeights) {

    private val verifiedWeightHashes: HashMap<Int, Any> = HashMap()
    private val verifiedItemHashes: HashMap<Int, Any> = HashMap()

    /**
     * These functions let you pick from this [MemoWeightedTable] instance's [items] with the given [customWeights].
     * NOTE: The order of [customWeights] should be according to this [MemoWeightedTable] instance's [items] order AFTER initial weight sorting.
     * e.g. Items = [1, 2, 3, 4, 5], Weights = [5, 4, 3, 2, 1] --put in to WeightedTable--> [items] = [5, 4, 3, 2, 1], [weights] = [1, 2, 3, 4, 5]
     */
    @JvmOverloads
    fun pickItemWithWeights(modifier: Int=0, customWeights: IntArray?=null): T {
        val selectedWeightRange: RandomRange<Long>
        val selectedWeightTable: IntArray
        if (customWeights is IntArray && customWeights.size == this.initialWeights.size) {
            if (customWeights.hashCode() !in this.verifiedWeightHashes) {
                customWeights.assessWeightStatus (
                    onValid= {
                        this@MemoWeightedTable.verifiedWeightHashes.putIfAbsent(customWeights.hashCode(), RandomRange(1L, customWeights.longSum(), true))
                    },
                    onInvalid= {
                        this@MemoWeightedTable.verifiedWeightHashes.putIfAbsent(customWeights.hashCode(), this)
                        throw WeightException(message)
                    },
                    onDuplicate= {
                        this@MemoWeightedTable.verifiedWeightHashes.putIfAbsent(customWeights.hashCode(), this)
                        throw WeightException(message)
                    }
                )
            } else {
                if (this.verifiedWeightHashes[customWeights.hashCode()]!! !is RandomRange<*>) {
                    throw WeightException((this.verifiedWeightHashes[customWeights.hashCode()] as WeightStatus).message)
                }
            }
            selectedWeightRange = this.verifiedWeightHashes[customWeights.hashCode()]!! as RandomRange<Long>
            selectedWeightTable = customWeights
        } else {
            selectedWeightRange = this.weightRange
            selectedWeightTable = this.initialWeights
        }
        var modifiedWeight = selectedWeightRange.pickItem() * (1.0 / (1 + (modifier.toDouble() / 100)))
        for ((weightIndex, weightValue) in selectedWeightTable.withIndex()) {
            modifiedWeight -= weightValue
            if (modifiedWeight <= 0)  {
                return this[weightIndex]
            }
        }
        return this[-1]
    }

    @JvmOverloads
    fun pickMapWithWeights(pickAmount: Int, modifier: Int=0, customWeights: IntArray?=null): Map<T, Int> {
        val pickMapWithWeights = HashMap<T, Int>()
        var result: T
        var previousCount: Int
        for (run in 0 until pickAmount) {
            result = this.pickItemWithWeights(modifier=modifier, customWeights=customWeights)
            pickMapWithWeights.putIfAbsent(result, 0)
            previousCount = pickMapWithWeights[result] as Int
            pickMapWithWeights[result] = previousCount + 1
        }
        return pickMapWithWeights
    }

    @JvmOverloads
    fun pickOrderedWithWeights(pickAmount: Int, modifier: Int=0, customWeights: IntArray?=null): List<T> {
        val resultsList = ArrayList<T>(pickAmount)
        for (listIndex in 0 until pickAmount)
            resultsList[listIndex] = this.pickItemWithWeights(modifier=modifier, customWeights=customWeights)
        return resultsList
    }

    /**
     * These functions let you pick from the given [customItems] with this [MemoWeightedTable] instance's [weights].
     * NOTE: The order of [customItems] should be according to this [MemoWeightedTable] instance's [weights] order AFTER initial weight sorting.
     * e.g. Items = [1, 2, 3, 4, 5], Weights = [5, 4, 3, 2, 1] --put in to WeightedTable--> [items] = [5, 4, 3, 2, 1], [weights] = [1, 2, 3, 4, 5]
     */
    @JvmOverloads
    fun pickItemFromItems(modifier: Int=0, customItems: Array<T>?=null): T {
        val selectedItemTable: List<T>
        if (customItems is Array<T> && customItems.size == this.initialWeights.size) {
            if (customItems.contentHashCode() !in this.verifiedItemHashes) {
                customItems.assessItemStatus (
                    onValid= {
                        this@MemoWeightedTable.verifiedItemHashes.putIfAbsent(customItems.contentHashCode(), true)
                    },
                    onInvalid= {
                        this@MemoWeightedTable.verifiedItemHashes.putIfAbsent(customItems.contentHashCode(), this)
                        throw WeightException(message)
                    }
                )
            } else {
                if (this.verifiedItemHashes[customItems.contentHashCode()]!!  != true) {
                    throw WeightException((this.verifiedItemHashes[customItems.contentHashCode()] as WeightStatus).message)
                }
            }
            selectedItemTable = customItems.toList()
        } else {
            selectedItemTable = this.itemList
        }
        var modifiedWeight = this.weightRange.pickItem() * (1.0 / (1 + (modifier.toDouble() / 100)))
        for ((weightIndex, weightValue) in this.initialWeights.withIndex()) {
            modifiedWeight -= weightValue
            if (modifiedWeight <= 0)  {
                return selectedItemTable[weightIndex]
            }
        }
        return this[-1]
    }

    @JvmOverloads
    fun pickMapWithItems(pickAmount: Int, modifier: Int=0, customItems: Array<T>?=null): Map<T, Int> {
        val pickMapWithWeights = HashMap<T, Int>()
        var result: T
        var previousCount: Int
        for (run in 0 until pickAmount) {
            result = this.pickItemFromItems(modifier=modifier, customItems=customItems)
            pickMapWithWeights.putIfAbsent(result, 0)
            previousCount = pickMapWithWeights[result] as Int
            pickMapWithWeights[result] = previousCount + 1
        }
        return pickMapWithWeights
    }

    @JvmOverloads
    fun pickOrderedWithItems(pickAmount: Int, modifier: Int=0, customItems: Array<T>?=null): List<T> {
        val resultsList = ArrayList<T>(pickAmount)
        for (listIndex in 0 until pickAmount)
            resultsList[listIndex] = this.pickItemFromItems(modifier=modifier, customItems=customItems)
        return resultsList
    }

    /**
     * This checks whether or not the array being checked meets the criteria for being picked from.
     */
    internal fun Array<T>.assessItemStatus (
        onValid: WeightStatus.()->Unit={},
        onInvalid: WeightStatus.()->Unit={}
    ) {
        if (this.size != this@MemoWeightedTable.initialWeights.size) {
            onInvalid(WeightStatus.INVALID_LENGTH)
            return
        }
        onValid(WeightStatus.VALID_WEIGHTS)
    }
}
