/**
 * @author: nbness2
 */

/**
 * [WeightedTable] is exactly what it looks like.
 * Every item put in to [items] will be assigned an [Int] weight, and will have a (weight/totalWeight) chance to be picked.
 * Weights are instantiated with [Int] weights because if you need any higher weights than 2.1B you need to look in to reducing those proportions
 * Example: ([[a, 20], [b, 10]] to [[a, 2], [b, 1]], same proportions smaller numbers)
 */
open class WeightedTable<T>(items: Array<T>, private val weights: IntArray) : RandomTable<T>(items) {

    internal val totalWeight: Long
    internal val weightRange: RandomRange<Long>

    init {
        weights.assessWeightStatus (
            onValid= { this@WeightedTable.sortItemsBy(weights) },
            onInvalid= { throw WeightException(message) },
            onDuplicate= { throw WeightException(message) }
        )
        this.sortItemsBy(weights)
        this.totalWeight = weights.longSum()
        this.weightRange = RandomRange(1L, this.totalWeight, true)
    }

    /**
     * [chanceOf] returns what the statistical chance of picking [item] from this [WeightedTable]
     */
    fun chanceOf(item: T): Double =
        if (item in this)
            (this.weights[this.getIndexOf(item)].toDouble() * 100) / this.totalWeight
        else
            0.0

    /**
     * The juice of the [WeightedTable].
     * This overrides [RandomTable]s [pickInternal], therefore can be called with the normal [pickItem], [pickMap] and [pickOrdered] functions.
     * This picks an item based on its weight using a pretty simply understood forumla for picking items based on weight.
     * If the modifier is invalid, this function will return the item with the highest chance of being picked.
     * The modifier can be seen as a percentage of how much more often the smallest weighted item (the one that will be picked the least) will be picked.
     * This leeches away from the larger weighted items until eventually only the smallest weighted item is able to be picked.
     * E.G. if the smallest weighted item is picked on average 100 times in X with modifier=0, the smallest weighted item would be picked 115 times in X with modifier=15.
     */
    override fun pickInternal(modifier: Int): T {
        var pickedWeight = this.weightRange.pickItem() * (1.0 / (1 + (modifier / 100)))
        for ((weightIndex, weightValue) in this.weights.withIndex()) {
            pickedWeight -= weightValue
            if (pickedWeight <= 0) return this[weightIndex]
        }
        return this[-1]
    }

    /**
     * [WeightException] is just a renamed exception. Conveys information better.
     */
    internal class WeightException(message: String): Exception(message)

    /**
     * [WeightStatus] is an enum that holds the error message values.
     */
    internal enum class WeightStatus(val message: String = "") {
        DUPLICATE_WEIGHT("Cannot have more than 1 item per weight."),
        INVALID_WEIGHT("Weight canot be less than 1."),
        VALID_WEIGHTS("Nothing to see here."),
        DUPLICATE_ITEM("Cannot have more than item per table"),
        INVALID_LENGTH("Weights and Items are not the same length")
        ;
    }

    /**
     * [assessWeightStatus] is used to check whether or not the weights being assessed are valid
     */
    internal fun IntArray.assessWeightStatus(
        onValid: WeightStatus.()->Unit={},
        onInvalid: WeightStatus.()->Unit={},
        onDuplicate: WeightStatus.()->Unit={}
    ) {
        // both weight and item array sizes must be the same
        if (this.size != itemList.size) {
            onInvalid(WeightStatus.INVALID_LENGTH)
            return
        }
        forEach { weight ->
            // you can only have unique weights, this is to avoid ambiguity as well as encourage putting items with the same weight in the same item
            if (count { it == weight } > 1) {
                onDuplicate(WeightStatus.DUPLICATE_WEIGHT)
                return
            // weights cannot be less than 1, this would kinda mess up the formula as well as make no sense to even have.
            } else if (weight < 1) {
                onInvalid(WeightStatus.INVALID_WEIGHT)
                return
            }
        }
        onValid(WeightStatus.VALID_WEIGHTS)
    }

    /**
     * [longSum] is an extension function complementary to [sum] that accumulates in a long
     * This is just in case the sum is bigger than [Int.MAX_VALUE]
     */
    internal fun IntArray.longSum(): Long {
        var longTotal = 0L
        for (value in this)
            longTotal += value
        return longTotal
    }
}

fun main() {
    val arr = intArrayOf(4, 2, 0, 6, 9)
}