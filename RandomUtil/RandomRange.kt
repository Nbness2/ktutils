/**
 * @author: nbness2
 */

/**
 * [RandomRange] is a wrapper for [BaseRandom] that only picks in a range specified at object instantiation.
 */
data class RandomRange <T>(val minimum: T, val maximum: T, private val inclusive: Boolean): BaseRandom<T>() where T: Number {
    /**
     * [pick]s a number between [minimum] and [maximum].
     * [inclusive] for Integrals (byte short int long) is whether or not the MAX end is included
     */
    fun pick(inclusive: Boolean): T =
        when (minimum) {
            is Byte,
            is Short ->
                this.nextInt(minimum.toInt(), maximum.toInt(), inclusive) as T

            is Int,
            is Long ->
                this.nextLong(minimum.toLong(), maximum.toLong(), inclusive) as T

            is Float ->
                this.nextFloat(minimum, maximum as Float) as T

            is Double ->
                this.nextDouble(minimum, maximum as Double) as T

            else ->
                throw IllegalAccessError("number type ${minimum::class.qualifiedName} not supported")
        }

    /**
     * [pick]s a number between [minimum] and [maximum] that uses [inclusive] as default
     */
    override fun pickItem(): T = this.pick(this.inclusive)
    override fun copyOf() = RandomRange(minimum, maximum, inclusive)
    override fun toString(): String = "($minimum -> $maximum)[$inclusive]"
}