package BoolUtil

/**
 * @Author: nbness2
 */

/**
 * This is akin to a more memory-conscious but less performant version of [kotlin.BooleanArray].
 * This uses 1 bit per boolean (and also 1 long per 64 bits), while [kotlin.BooleanArray] uses 1 byte per boolean
 */
class BoolArray(val size: Int): Iterator<Boolean> {

    /**
     * This constructor is the constructor with the lambda you can initialize from
     * Example:
     *      val boolArray = BoolArray(10) { it.isEven() }
     *      println(boolArray.contentToString()) // should print: [true, false, true, false, true, false, true, false, true, false]
     */
    constructor(size: Int, init: (index: Int) -> Boolean): this(size) {
        (0 until size).forEach { index ->
            this[index] = init(index)
        }
    }

    /**
     * Where all the storage magic happens in [Long]s. [Long] is used instead of [Byte] because every object has the same reference overhead so [Long] gives you the most bang for your buck no matter how you see it.
     * Minimum object size on JVM is 32 bits, so even separate bytes and booleans use at least 4 bytes each. With [BoolArray] each [Boolean] uses 1 bit, so theres at least a 31 bit saving for each [Boolean] in this array.
     */
    private val internalLongs = LongArray(if (this.size > 0) ((this.size-1)/Long.SIZE_BITS)+1 else 0)

    /**
     * This is the max number you can put in to [BoolArray.get] and [BoolArray.set]
     */
    private val maxBitIndex: Int
        get() = size-1

    /**
     * This is the counter for detecting when iteration is over
     */
    private var currentIndex = 0

    /**
     * This is the first part of the iterator that tells the for loops if it can continue
     */
    override fun hasNext(): Boolean {
        if (currentIndex >= this.size) {
            currentIndex = 0
            return false
        }
        return true
    }

    /**
     * This is the second part of the iterator that gives the for loops their values
     */
    override fun next(): Boolean = this[currentIndex++]

    /**
     * This is the magic that lets you get the [Boolean] from the long at [index]
     */
    operator fun get(index: Int): Boolean {
        return ((this.internalLongs[index / Long.SIZE_BITS] and -1L) and (0b1L shl (index % Long.SIZE_BITS))) != 0L
    }

    /**
     * This checks if the given boolean is present in this [BoolArray].
     */
    operator fun contains(element: Boolean): Boolean = this.any { it == element }

    /**
     * This checks if all of the given booleans are present in this [BoolArray]
     */
    fun containsAll(elements: Collection<Boolean>): Boolean {
        for (element in elements) {
            if (element !in this)
                return false
        }
        return true
    }

    /**
     * Supplementary functions to allow for slice notation when getting items from this array
     */
    private fun IntRange.toIntArray(): IntArray {
        val returnList = mutableListOf<Int>()
        for (number in this)
            returnList.add(number)
        return returnList.toIntArray()
    }
    private fun IntProgression.toIntArray(): IntArray {
        val returnList = mutableListOf<Int>()
        for(number in this)
            returnList.add(number)
        return returnList.toIntArray()
    }
    operator fun get(indices: IntArray): BoolArray = BoolArray(indices.size) { this[indices[it]] }
    operator fun get(slice: IntRange): BoolArray = this[slice.toIntArray()]
    operator fun get(slice: IntProgression): BoolArray = this[slice.toIntArray()]

    /**
     * This is the magic that sets the [Boolean] value in the bit of a [Long]
     */
    operator fun set(index: Int, value: Boolean) {
        if (value)
            this.internalLongs[index / Long.SIZE_BITS] = ((this.internalLongs[index / Long.SIZE_BITS] or (1L shl (index % Long.SIZE_BITS))) and -1L)
        else
            this.internalLongs[index / Long.SIZE_BITS] = ((this.internalLongs[index / Long.SIZE_BITS] and (1L shl (index % Long.SIZE_BITS)).inv()) and -1L)
    }

    /**
     * Checks if this arrays size is 0 or less (shouldnt ever be less lol)
     */
    fun isEmpty(): Boolean = this.size <= 0

    /**
     * This prints the [Boolean] contents of the bits up to [maxBitIndex]
     */
    fun contentToString(): String {
        val outputString = StringBuilder("[")
        for (booleanValue in this.withIndex()) {
            outputString.append(booleanValue.value)
            if (booleanValue.index != this.maxBitIndex)
                outputString.append(", ")
        }
        return outputString.append("]").toString()
    }

    /**
     * predicate functions
     */
    inline fun all(crossinline predicate: (Boolean) -> Boolean): Boolean {
        for (boolean in this) if (!predicate(boolean)) return false
        return true
    }
    inline fun any(crossinline predicate: (Boolean) -> Boolean): Boolean {
        for (boolean in this) if (predicate(boolean)) return true
        return false
    }
}

/**
 * This is just to go along with the typeArrayOf trend
 */
fun boolArrayOf(vararg booleans: Boolean): BoolArray = BoolArray(booleans.size) { booleans[it] }

/**
 * These are extension functions made to give them more of a natural feel
 */
fun BoolArray?.isNullOrEmpty(): Boolean = this?.isEmpty() ?: true
fun BoolArray.toBooleanArray(): BooleanArray = BooleanArray(this.size) { this[it] }
fun BoolArray.toTypedArray(): Array<Boolean> = Array(this.size) { this[it] }
fun BoolArray.toNullableTypedArray(): Array<Boolean?> = Array(this.size) { this[it] }
fun BoolArray.toList(): List<Boolean> = List(this.size) { this[it] }

fun main() {
    val myArray = boolArrayOf(true, false, true, false, true, false, true, false, true, false, true, false, true, false, true, false, true, false, true, false, true, false, true, false, true, false)
    val sliced = myArray[0 until myArray.size step 5]
    myArray.iterator()
    println(myArray.contentToString())
    println(sliced.contentToString()) // should print: [true, false, true, false, true, false]
}