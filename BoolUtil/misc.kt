package BoolUtil

/**
 * @Author: nbness2
 */

/**
 * This converts your boolean to an integer
 */
fun Boolean.toInt(): Int = if (this) 1 else 0

/**
 * These are just supplementary boolean collection-y functions that can help with some stuff here and there
 */
fun BooleanArray.allEqual(): Boolean = if (this.isEmpty()) false else this.all { it == this[0] }
fun Array<Boolean>.allEqual(): Boolean = if (this.isNullOrEmpty()) false else this.all { it == this[0] }
fun List<Boolean>.allEqual(): Boolean = if (this.isNullOrEmpty()) false else this.all { it == this[0] }
fun Collection<Boolean>.allEqual(): Boolean = if (this.isNullOrEmpty()) false else this.all { it == this.first() }
fun BoolArray.allEqual(): Boolean = if (this.isEmpty()) false else this.all { it == this[0] }
