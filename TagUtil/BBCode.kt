package TagUtil

val TEXTBOX = buildTagBase{}().usingClosingTag(false).usingOpeningTag(false)
val tagBase = buildTagBase { tagBrackets = "[]" }
val quotelessTag = tagBase { putTagValueInQuotes = false }

val fontName = quotelessTag.withTagType("font")
val fontSize = quotelessTag.withTagType("size")
val fontColor = quotelessTag.withTagType("color")
val boldText = quotelessTag.withTagType("b")
val italicText = quotelessTag.withTagType("i")
val underlineText = quotelessTag.withTagType("u")
val slashText = quotelessTag.withTagType("s")
val subText = quotelessTag.withTagType("sub")
val superText = quotelessTag.withTagType("sup")
val alignLeft = quotelessTag.withTagType("left")
val alignCenter = quotelessTag.withTagType("center")
val alignRight = quotelessTag.withTagType("right")
val listTag = quotelessTag.withTagType("list")
val listItem = quotelessTag.withTagType("*").usingClosingTag(false)
val indent = quotelessTag.withTagType("indent")
val urlWithText = tagBase { tagType = "url" }
val justUrl = quotelessTag.withTagType("url")
val email = tagBase { tagType = "email" }
val image = quotelessTag.withTagType("img")
val video = quotelessTag.withTagType("video")
val quote = quotelessTag.withTagType("quote")
val code = quotelessTag.withTagType("code")
val spoiler = tagBase { tagType = "spoiler" }
val edit = quotelessTag.withTagType("edit")
val google = quotelessTag.withTagType("google")
val user = quotelessTag.withTagType("user")
val wiki = quotelessTag.withTagType("wiki")
val horizontalLine = quotelessTag.withTagType("hr")


val FINAL_TEXT = TEXTBOX {
    +"This was created in Kotlin 1.3, so I cannot guarantee compatability with any kotlin version released beforehand"
    +"Hey guys, nbness here. Today, I'm writing all this whole post with a basic wrapper DSL included with the rest of this download, which will also include this post as an example in \"BBCodeExample.kt\""
    +spoiler { tagValue = "code examples"
        +fontColor { tagValue = "FF0000"
            +"this is regular FF0000 text"
            +fontName { tagValue = "Comic Sans MS"
                +"this red Comic Sans MS text"
            }
        }
        -fontSize { tagValue = "6"
            +"this is size 6 regular text"
            -fontName { tagValue = "fixedsys"
                +"this is size 6 fixedsys text"
                -fontColor { tagValue = "BLUE"
                    +"and this is size 6 blue fixedsys text"
                }
            }
        }

        +"this is a google search for: "
        -google("google")
        -" using the google tag"

        +"this is a wikipedia article on birds using the wikipedia tag: "
        -wiki("birds")

        +alignLeft(fontColor("here is some 00ff00 left-aligned text") { tagValue = "00FF00" })
        +alignCenter("Here is some uncoloured centered text")
        +alignRight {
            +"Here one sentence using right-aligned text"
            +"Here is another sentence using right-aligned text in the same tag"
        }

        +"here is a link to my rune-server user page using the user tag -> "
        -user("nbness2")

        +superText("Here is super text on the")
        -"same line as regular as"
        -subText("well as sub text")

        +"Heres a real slapper of a song, im just using this video as an example of the video tag i swear to scu im not a weeb oh fuck what will my waifu say when she finds out"
        +video("https://www.youtube.com/watch?v=mazFQ1AJaWE")
    }

    +"I have had some down-time of not being able to work on my project because I had to undergo surgery, and I can't bring my desktop to the hospital as far as I know, so I decided to make use of my laptop and write these because I will be using them all in my project: "
    -urlWithText("NBX") { tagValue = "https://www.rune-server.ee/runescape-development/rs-503-client-server/projects/678552-nbx-breathing-new-life-pvp.html" }

    +"I have already found use cases for all of them in my project, so I am releasing hem in hopes that others may find them useful and can learn from them!"
    +"Here's a numbered list of utils."
    +listTag { tagValue = "1"
        +listItem("BoolUtil includes") {
            +listTag { wrapTag = listItem
                +"Alternative BoolArray (BooleanArray) that uses 1 bit per boolean rather than 1 byte, so space is saved using large boolean arrays but comes at the cost of performance."
                +"BooleanArray operates identical to BooleanArray and Array<Boolean> in terms of set/get and initialization"
                +"this also includes a very small amount of other misc boolean-collection related functions"
            }
        }
        +listItem("ColourUtil includes") {
            +listTag { wrapTag = listItem
                +"140 default colours to pick from (parsed frrom "
                -justUrl("https://htmlcolorcodes.com/color-names/")
                +")"
                +"You can also make your own colours if you want to, of course"
            }
        }
        +listItem("NumberUtil inclues: ") {
            +listTag { wrapTag = listItem
                +"converting primitive numbers to and from ByteArrays"
                +"Converting primitive numbers to hex"
                +"Checking if primitive numbers are even or odd"
            }
        }
        +listItem("TextUtil includes: ") {
            +listTag { wrapTag = listItem
                +"String manipulation"
                +"English checking functions"
                +"Capitalization, symbolic and numeric checking functions"
            }
        }
        +listItem("DelegateUtil") {
            +listTag { wrapTag = listItem
                +"This is a flexible variable delegate that you can do quite much with, the limit is your imagination!"
            }
        }
    }

    +spoiler { tagValue = "pictures"; wrapTag = image
        !"Default colours"
        +"https://i.imgur.com/Q9Lsn7S.png"

        !"Delegate example"
        +"https://i.imgur.com/yJCFhOw.png"

        !"To and From ByteArray. Number's primitive subclasses (Int, Short, etc.) all have their own .toByteArray that output the same thing as Number.toByteArray, but I couldn't do the same Number.fromByteArray because Number doesn't have a companion object and it would've looked ugly as shit"
        +"https://i.imgur.com/QtMdPm6.png"
        +"https://i.imgur.com/vkWb8qo.png"

        !"BoolArray. This stores the bool values in bits rather than bytes, so instead of 64 bytes per 64 booleans its 8 bytes per 64 booleans (uses long for boolean storage). BoolArray works identically to BooleanArray and Array<Boolean> in terms of get/set and initialization, havent gone too deep to make it copy paste-able in every case though it should be in most cases"
        +"https://i.imgur.com/d0vhISQ.png"

        !"DSL example"
        +"https://i.imgur.com/Fbz3EdO.png"
    }

    +"Git link: "
    -justUrl("https://bitbucket.org/Nbness2/ktutils/src/master/")
}

val changelog20181122 = TEXTBOX {
    +"Changes to Tags and TagBuilders"
    +listTag { wrapTag = listItem
        +"No more specifying when to use newlines. Newlines are now ALWAYS added BEFORE a string (+\"string\") when using builder syntax. I decided to go with this rather than explicitly specifying automatic newlines because it honestly makes more visual sense."
        +"Because of the above change, NewlineIdentifier class (the gay ass bool wrapper) has been removed"

        +"One-liner tags are easier to create. Now you would use something like "
        -italicText("listItem(\"http://www.google.com\")")
        -" rather than "
        -italicText("listItem { -newlineBefore; -newlineAfter; +\"http://www.google.com/\" }")

        +"The above change was enabled by adding an initialization parameter to FullTagBuilder as well as adding a few .invoke constructors to TypedTag"
        +"Fixed a bug with FullTag.toString(String) putting the given string in both the tag AND making it the tagged text. oops lol."
        +"Updated PairUtil.main post bbcode using the newer syntax. Shouldn't be able to see a difference!"
    }

    +"Gist link to DSL for this comment (requires updated Tags and TagBuilder which can be found at the bottom of the PairUtil.main post): "
    -justUrl("https://gist.github.com/nbness2/f0fcbb48bb74bfa7da3670b6b84f8ac0")

}

val changelog20181205 = TEXTBOX {
    +"Bigger than normal update on this project. I've been working on this for a joint college project. NBX updates soon hopefully."
    +"Updated the main post's DSL code as well as wording a bit."
    +"One thing I did across the whole project was document a lot. It might not be the best, but some documentation is better than none."
    +""

    +"TagUtil"
    +listTag { wrapTag = listItem
        +"Tags, TagBuilders, and my BBCode example is now in its own folder, TagUtil."
        +"BBCode examples also now better reflect the upgrades to the DSL"
        +"Updated features making the TagBuilder in to a slightly better DSL."
        !listTag { wrapTag = listItem
            +"You can now specify a tag you want to wrap for every added string immediately inside the current block by using \"outerTag { wrapTag = tagName }\" before adding strings. Example: "
            -justUrl("https://i.imgur.com/avRaZK0.png")
            +"This is useful so you dont have to copy paste a tag before every line for something like a spoiler full of images, or a list full of list items."
            +"2 new unary operators for adding strings. (+) will function as normal, (!) will add the string but not wrap the string with any tags (if wrapTag is given), (-) will continue the text on the previous line with NO wrapped tag. Example: "
            -justUrl("https://i.imgur.com/xIgXweI.png")
            +"The line continuation (-) operator works best with tags that open but don't close, such as listItem (in BBCode, listItem -> [*]text)"
        }
    }
    +""

    +"RandomUtil"
    +listTag { wrapTag = listItem
        +"Ths RandomUtil is very similar on the inside to my other RandomUtil, nearly identical on the outside."
        +"It is more simple on the inside because I didnt butcher it with useless reflection and special boi abstraction."
        +"Still gives identical (within margin of error bc random) results"
        +"Upgraded WeightedTable so it performs slightly better as well as is a bit more neat looking."
        +"With RandomUtil, there are also extensions to primitive types and String."
        +"Added MemoWeightedTable. You can use custom weights and custom items with these. They memoize the hashcodes of the custom arrays used so they perform better when used more than once."
    }
    +""

    +"TextUtil"
    +listTag {
        wrapTag = listItem
        +"String.rpad and String.lpad both do ALMOST the same thing as String.padStart and String.padEnd, except they don't chop the string off at the specified length"
        +"String.remove removes all exact instances of the given input string and returns the new \"cleaned\" version"
        +"String.translate uses TranslationTables (also included). String.translate(TranslationTable) maps all instances of any character in the given TranslationTable's inChars to that characters outChars index match"
        +"You can also use TranslationTable.translateString(String) to achieve the same result"
        +"You can also reverse translate using String.reverseTranslate(TranslationTable) and TranslationTable.reverseTranslateString(String)"
    }
    +""

    +"All I did with ColourUtil was remove the initializer in DefaultColour and make DefaultColour extend Colour"
    +""

    +"MatrixUtil"
    +listTag { wrapTag = listItem
        +"MatrixUtil contains Matrices for all the primitive types."
        +"You can add, multiply and subtract matrices with scalars and eligible matrices. cbf to do number array inverses because I can barely do that shit on a paper (and the way I got it working in code was terribly inefficient for larger than 5x5 arrays). Boolean array inverse just inverses all the boolean values though :D"
        +"You can transpose the matrices"
        +"I tried to make a Generic Matrix but there was an UnsupportedOperationException related to reified type params and inlining. wtf mang :'("
        +"I am going to keep working on Generic Matrix to somehow get this dumb bitch to work, and it will support custom addition, custom subtraction, custom scalar multiplication and custom dot products. Eventually."
    }
}

fun main() {
    println(FINAL_TEXT)
}