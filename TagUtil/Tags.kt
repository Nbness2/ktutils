package TagUtil

data class BaseTag(
    private val tagBrackets: String="<>"
) {
    fun openingTag(withTagType: String?=null, tagValue: String?=null): String {
        val openingTag = StringBuilder()
        openingTag.append(tagBrackets[0])
        openingTag.append(withTagType ?: "%s") // tagType goes here
        if (tagValue != null && tagValue.isNotEmpty()) {
            openingTag.append("=")
            openingTag.append(tagValue)
        }
        openingTag.append(tagBrackets[1])
        return openingTag.toString()
    }

    fun closingTag(withTagType: String?=null): String {
        val closingTag = StringBuilder()
        closingTag.append(tagBrackets[0])
        closingTag.append("/")
        closingTag.append(withTagType ?: "%s")
        closingTag.append(tagBrackets[1])
        return closingTag.toString()
    }

    fun withTagBrackets(newTagBrackets: String): BaseTag = this.copy(tagBrackets=newTagBrackets.slice(0..1))
    fun withTagBrackets(newTagBrackets: String, build: TypedTagBuilder.() -> Unit): TypedTag = this.copy(tagBrackets=newTagBrackets.slice(0..1)).invoke(build)

    operator fun invoke(build: TypedTagBuilder.() -> Unit): TypedTag = TypedTagBuilder().apply(build).buildTypedTag(this)
    operator fun invoke(): TypedTag = TypedTagBuilder().apply{}.buildTypedTag(this)
}

data class TypedTag(private val tagBase: BaseTag, private val tagType: String="TAG_TYPE", private val useOpeningTag: Boolean=true, private val useClosingTag: Boolean=true, private val putTagValueInQuotes: Boolean=false) {
    fun openingTag(tagValue: String?=null): String {
        if (!useOpeningTag)
            return ""
        if (tagValue != null && putTagValueInQuotes)
            return tagBase.openingTag(tagType, "\"$tagValue\"")
        return tagBase.openingTag(tagType, tagValue)
    }

    fun closingTag(): String {
        if (!useClosingTag)
            return ""
        return tagBase.closingTag(tagType)
    }

    fun toString(withText: String?=null): String {
        val finalString = StringBuilder()
        finalString.append(this.openingTag())
        finalString.append(withText)
        finalString.append(this.closingTag())
        return finalString.toString()
    }

    override fun toString(): String {
        return this.toString("%s")
    }
    fun withTagType(newTagType: String): TypedTag = this.copy(tagType=newTagType)
    fun withTagType(newTagType: String, build: FullTagBuilder.() -> Unit): FullTag = this.copy(tagType=newTagType).invoke(build)

    fun withTagBase(newTagBase: BaseTag): TypedTag = this.copy(tagBase=newTagBase)
    fun withTagBase(newTagBase: BaseTag, build: FullTagBuilder.() -> Unit): FullTag = this.copy(tagBase=newTagBase).invoke(build)

    fun usingOpeningTag(newUseOpeningTag: Boolean): TypedTag = this.copy(useOpeningTag=newUseOpeningTag)
    fun usingOpeningTag(newUseOpeningTag: Boolean, build: FullTagBuilder.() -> Unit): FullTag = this.copy(useOpeningTag=newUseOpeningTag).invoke(build)

    fun usingClosingTag(newUseClosingTag: Boolean): TypedTag = this.copy(useClosingTag=newUseClosingTag)
    fun usingClosingTag(newUseClosingTag: Boolean, build: FullTagBuilder.() -> Unit): FullTag = this.copy(useClosingTag=newUseClosingTag).invoke(build)

    fun whilePuttingTagValueInQuotes(newPutTagValueInQuotes: Boolean): TypedTag = this.copy(putTagValueInQuotes=newPutTagValueInQuotes)
    fun whilePuttingTagValueInQuotes(newPutTagValueInQuotes: Boolean, build: FullTagBuilder.() -> Unit): FullTag = this.copy(putTagValueInQuotes=newPutTagValueInQuotes).invoke(build)

    operator fun invoke(initialString: String, build: FullTagBuilder.() -> Unit) : FullTag = FullTagBuilder(initialString).apply(build).buildFinalTag(this)
    operator fun invoke(build: FullTagBuilder.() -> Unit): FullTag = FullTagBuilder().apply(build).buildFinalTag(this)
    operator fun invoke(taggedText: String): String = this.toString(withText=taggedText)
    operator fun invoke(fullTag: FullTag): String = this.toString(fullTag.toString())
    operator fun invoke(): FullTag = FullTagBuilder().apply{}.buildFinalTag(this)
}

data class FullTag(private val tagType: TypedTag, private val tagValue: String?=null, private val taggedText: String="", private val wrapWithTag: TypedTag?=null) {
    fun openingTag(withValue: String?=null): String = this.tagType.openingTag(withValue ?: this.tagValue)
    fun closingTag(): String = this.tagType.closingTag()
    fun withTagValue(newTagValue: String): FullTag = this.copy(tagValue=newTagValue)
    fun withTaggedText(newTaggedText: String): FullTag = this.copy(taggedText=newTaggedText)
    override fun toString(): String {
        val finalString = StringBuilder()
        finalString.append(this.openingTag())
        finalString.append(this.taggedText)
        finalString.append(this.closingTag())
        return finalString.toString()
    }
}
