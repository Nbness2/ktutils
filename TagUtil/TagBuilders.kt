package TagUtil

/**
 * These are supplementary functions made for condensing strings and tags together on the same line without being too much of a hassle
 */
operator fun String.plus(other: Any?): String = this + other.toString()
operator fun FullTag.plus(other: String): String = this.toString() + other

/**
 * This is the base tag builder, usually only what defines the brackets that the tag system uses and in some cases only switching the brackets will make a valid tag in another tag system
 */
class BaseTagBuilder {
    var tagBrackets: String = "  "
    fun buildBaseTag(): BaseTag = BaseTag(tagBrackets=tagBrackets.slice(0 .. 1))
}

/**
 * This is the builder for [BaseTag]
 */
fun buildTagBase(builder: BaseTagBuilder.() -> Unit): BaseTag = BaseTagBuilder().apply(builder).buildBaseTag()

/**
 * This is the intermediate builder, this gives [TypedTag] which is right above the foundation of the tag system.
 */
// TODO: Move useOpening, useClosing and putTagValueInQuotes from BaseTag to TypedTag
class TypedTagBuilder {
    var tagType: String = ""
    var useOpeningTag: Boolean = true // This says whether or not the tag is used opening. Example for false: taggedText [/tagType]
    var useClosingTag: Boolean = true // This says whether or not the tag is used closing. Example for true: [tagType]taggedText[/tagType]
    var putTagValueInQuotes: Boolean = false // This says whether or not the tag's value when given is wrapped in quotes. example for true: <color="FF0000">RED</color>
    fun buildTypedTag(withBase: BaseTag): TypedTag = TypedTag(tagBase=withBase, tagType=tagType, useOpeningTag=useOpeningTag, useClosingTag=useClosingTag, putTagValueInQuotes=putTagValueInQuotes)
}

/**
 * This is where all the magic happens in terms of creating the text
 */
class FullTagBuilder(initialString: String="", var wrapTag: TypedTag?=null) {
    // This is the stringbuilder that gives the final result for the FullTag
    private val internalStringBuilder = StringBuilder(initialString)

    /**
     * This is where the magic happens. [unaryPlus] allows for the cool looking syntax.
     * If [wrapTag] is given and is not null, it will wrap the string using [wrapTag]
     * Example: +"Hello there" // appends "Hello there" to [internalStringBuilder]
     * Example: + tagName { +"tagged text" } // appends "<tagName>tagged text</tagName>" to [internalStringBuilder] assuming tagName tag has <> brackets and uses opening and closing tags.
     */
    operator fun String.unaryPlus() {
        internalStringBuilder.append("\n")
        internalStringBuilder.append(wrapTag?.invoke(this) ?: this)
    }
    operator fun TypedTag.unaryPlus() { +this.toString() }
    operator fun FullTag.unaryPlus() { +this.toString() }

    /**
     * This will pls ONLY the string whether or not [wrapTag] is given.
     */
    operator fun String.unaryMinus() { internalStringBuilder.append(this) }
    operator fun TypedTag.unaryMinus() { -this.toString() }
    operator fun FullTag.unaryMinus() { -this.toString() }

    /**
     * This will pls a newline and the string but NOT (see what i did there) wrap it in tag.
     */
    operator fun String.not() {
        internalStringBuilder.append("\n")
        internalStringBuilder.append(this)
    }
    operator fun TypedTag.not() { !this.toString() }
    operator fun FullTag.not() { !this.toString() }

    /**
     * This can be used just to set the tag value when turning in to a string
     * Example: FullTagName{tagValue="tagValue"} // will give <FullTagName=TagValue></FullTagName> assuming BaseTag has <> as brackets, uses opening and closing tags with no quote wrapper around tag value
     */
    var tagValue: String = ""
    /**
     * This can be used if you only want 1 line of text and nothing too confusing to read
     * Example: FullTagName{taggedText="tagged text"} // will give <FullTagName>tagged text</FullTagName> assuming BaseTag has <> as brackets, uses opening and closing tags.
     */
    var taggedText: String = ""
    fun buildFinalTag(withTagType: TypedTag): FullTag = FullTag(tagType=withTagType, tagValue=tagValue, taggedText=if (taggedText.isEmpty()) internalStringBuilder.toString() else taggedText)
}

fun main() {
    println("a")
}