package ColourUtil

/**
 * @Author: nbness2
 */

open class Colour(hexValue: String) {
    private fun String.verifyIsHex(): Boolean =
        this.all { character ->
            character in charArrayOf('0', '1', '2', '3', '4', '5', '6', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F')
        }

    val hexValue: String
    val intValue: Int

    init {
        if (hexValue.verifyIsHex()) {
            this.hexValue = hexValue
            this.intValue = hexValue.toInt(16)
        } else
            throw Exception("Invalid string for Colour.hexValue: $hexValue")
    }
}