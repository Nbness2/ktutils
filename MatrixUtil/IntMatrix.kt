package MatrixUtil

class IntMatrix(val rows: Int, val columns: Int, init: (Int) -> IntArray={ IntArray(columns) { 0 } }) {
    private val internalRows = Array(rows) {
        val arrayToAdd = init(it)
        if (arrayToAdd.size != columns) throw IndexOutOfBoundsException("initialized arrays cannot be bigger than $columns")
        arrayToAdd.copyOf()
    }

    constructor(init: Array<IntArray>): this(init.size, init.first().size, { init[it].copyOf() } )

    operator fun get(rowIndex: Int): IntArray = this.internalRows[rowIndex].copyOf()
    operator fun get(rowIndex: Int, columnIndex: Int): Int = this.internalRows[rowIndex][columnIndex]
    operator fun set(rowIndex: Int, other: IntArray) { this.internalRows[rowIndex] = other.copyOf() }
    operator fun set(rowIndex: Int, columnIndex: Int, other: Int) { this.internalRows[rowIndex][columnIndex] = other }

    operator fun plus(other: Int): IntMatrix {
        val newIntMatrix = IntMatrix(this.rows, this.columns)
        for (row in this.internalRows.withIndex())
            for (int in row.value.withIndex())
                newIntMatrix[row.index, int.index] = this[row.index, int.index] + other
        return newIntMatrix
    }
    operator fun plus(other: IntMatrix): IntMatrix {
        if (this dimensionsEqual other) {
            val newIntMatrix = this.copy()
            for (row in this.internalRows.withIndex())
                for (int in row.value.withIndex())
                    newIntMatrix[row.index, int.index] = (newIntMatrix[row.index, int.index] + other[row.index, int.index]).toInt()
            return newIntMatrix
        }
        throw Exception("Matrix dimensions do not match, cannot pls together.")
    }
    operator fun plusAssign(other: Int) {
        for (row in this.internalRows.withIndex())
            for (int in row.value.withIndex())
                this[row.index, int.index] = int.value + other
    }
    operator fun plusAssign(other: IntMatrix) {
        if (this dimensionsEqual other) {
            for (row in this.internalRows.withIndex())
                for (int in row.value.withIndex())
                    this[row.index, int.index] = this[row.index, int.index] + other[row.index, int.index]
            return
        }
        throw Exception("Matrix dimensions do not match, cannot pls together.")
    }

    operator fun minus(other: Int): IntMatrix {
        val newIntMatrix = IntMatrix(this.rows, this.columns)
        for (row in this.internalRows.withIndex())
            for (int in row.value.withIndex())
                newIntMatrix[row.index, int.index] = this[row.index, int.index] - other
        return newIntMatrix
    }
    operator fun minus(other: IntMatrix): IntMatrix {
        if (this dimensionsEqual other) {
            val newIntMatrix = this.copy()
            for (row in this.internalRows.withIndex())
                for (int in row.value.withIndex())
                    newIntMatrix[row.index, int.index] = newIntMatrix[row.index, int.index] - other[row.index, int.index]
            return newIntMatrix
        }
        throw Exception("Matrix dimensions do not match, cannot subtract.")
    }
    operator fun minusAssign(other: Int) {
        for (row in this.internalRows.withIndex())
            for (int in row.value.withIndex())
                this[row.index, int.index] = int.value - other
    }
    operator fun minusAssign(other: IntMatrix) {
        if (this dimensionsEqual other) {
            for (row in this.internalRows.withIndex())
                for (int in row.value.withIndex())
                    this[row.index, int.index] = this[row.index, int.index] - other[row.index, int.index]
            return
        }
        throw Exception("Matrix dimensions do not match, cannot subtract.")
    }

    operator fun times(other: Int): IntMatrix {
        val newIntMatrix = IntMatrix(this.rows, this.columns)
        for (row in this.internalRows.withIndex())
            for (int in row.value.withIndex())
                newIntMatrix[row.index, int.index] = this[row.index, int.index] * other
        return newIntMatrix
    }
    operator fun times(other: IntMatrix): IntMatrix {
        if (this.columns == other.rows) {
            val newIntMatrix = IntMatrix(this.rows, other.columns)
            var currentRow: IntArray
            for (rowIndex in 0 until this.rows) {
                currentRow = this.getRow(rowIndex)
                for (columnIndex in 0 until other.columns)
                    newIntMatrix[rowIndex, columnIndex] = currentRow dot other.getColumn(columnIndex)
            }
            return newIntMatrix

        }
        throw Exception("Matrix dimensions do not match for multiplication, cannot multiply together.")
    }
    operator fun timesAssign(other: Int) {
        for (row in this.internalRows.withIndex())
            for (int in row.value.withIndex())
                this[row.index, int.index] = int.value * other
    }

    operator fun div(other: Int): IntMatrix {
        val newIntMatrix = IntMatrix(this.rows, this.columns)
        for (row in this.internalRows.withIndex())
            for (int in row.value.withIndex())
                newIntMatrix[row.index, int.index] = (this[row.index, int.index] / other).toInt()
        return newIntMatrix
    }
    operator fun divAssign(other: Int) {
        for (row in this.internalRows.withIndex())
            for (int in row.value.withIndex())
                this[row.index, int.index] = int.value / other
    }

    infix fun dimensionsEqual(other: IntMatrix): Boolean = this.rows == other.rows && this.columns == other.columns
    fun transposed(): IntMatrix = IntMatrix(this.columns, this.rows) { this.getColumn(it) }
    fun getRow(rowIndex: Int) = this[rowIndex]
    fun getColumn(columnIndex: Int): IntArray = this.internalRows.map { it[columnIndex] }.toIntArray()
    fun copy(): IntMatrix = IntMatrix(this.rows, this.columns) { this.getRow(it) }
    override fun toString(): String = this.internalRows.joinToString("\n") { it.joinToString(prefix="[", postfix="]") }

    infix fun IntArray.dot(other: IntArray): Int =
        if (this.size == other.size)
            (this zip other).sumBy {ints -> (ints.first * ints.second) }.toInt()
        else
            throw Exception("Both arrays must be same length to dot product them")
}


fun main() {
    val mat1 = IntMatrix(
        arrayOf(
            intArrayOf(0, 3, 5),
            intArrayOf(50, 5, 2)
        )
    )

    val mat2 = IntMatrix(
        arrayOf(
            intArrayOf(3, 4),
            intArrayOf(3, -2),
            intArrayOf(4, -2)
        )
    )

    println(mat1)
    println()
    println(mat2)
    println()
    println(mat1*mat2)
}