package MatrixUtil

class LongMatrix(val rows: Int, val columns: Int, init: (Int) -> LongArray={ LongArray(columns) { 0 } }) {
    private val internalRows = Array(rows) {
        val arrayToAdd = init(it)
        if (arrayToAdd.size != columns) throw IndexOutOfBoundsException("initialized arrays cannot be bigger than $columns")
        arrayToAdd.copyOf()
    }

    constructor(init: Array<LongArray>): this(init.size, init.first().size, { init[it].copyOf() } )

    operator fun get(rowIndex: Int): LongArray = this.internalRows[rowIndex].copyOf()
    operator fun get(rowIndex: Int, columnIndex: Int): Long = this.internalRows[rowIndex][columnIndex]
    operator fun set(rowIndex: Int, other: LongArray) { this.internalRows[rowIndex] = other.copyOf() }
    operator fun set(rowIndex: Int, columnIndex: Int, other: Long) { this.internalRows[rowIndex][columnIndex] = other }

    operator fun plus(other: Long): LongMatrix {
        val newLongMatrix = LongMatrix(this.rows, this.columns)
        for (row in this.internalRows.withIndex())
            for (long in row.value.withIndex())
                newLongMatrix[row.index, long.index] = this[row.index, long.index] + other
        return newLongMatrix
    }
    operator fun plus(other: LongMatrix): LongMatrix {
        if (this dimensionsEqual other) {
            val newLongMatrix = this.copy()
            for (row in this.internalRows.withIndex())
                for (long in row.value.withIndex())
                    newLongMatrix[row.index, long.index] = (newLongMatrix[row.index, long.index] + other[row.index, long.index]).toLong()
            return newLongMatrix
        }
        throw Exception("Matrix dimensions do not match, cannot pls together.")
    }
    operator fun plusAssign(other: Long) {
        for (row in this.internalRows.withIndex())
            for (long in row.value.withIndex())
                this[row.index, long.index] = long.value + other
    }
    operator fun plusAssign(other: LongMatrix) {
        if (this dimensionsEqual other) {
            for (row in this.internalRows.withIndex())
                for (long in row.value.withIndex())
                    this[row.index, long.index] = this[row.index, long.index] + other[row.index, long.index]
            return
        }
        throw Exception("Matrix dimensions do not match, cannot pls together.")
    }

    operator fun minus(other: Long): LongMatrix {
        val newLongMatrix = LongMatrix(this.rows, this.columns)
        for (row in this.internalRows.withIndex())
            for (long in row.value.withIndex())
                newLongMatrix[row.index, long.index] = this[row.index, long.index] - other
        return newLongMatrix
    }
    operator fun minus(other: LongMatrix): LongMatrix {
        if (this dimensionsEqual other) {
            val newLongMatrix = this.copy()
            for (row in this.internalRows.withIndex())
                for (long in row.value.withIndex())
                    newLongMatrix[row.index, long.index] = (newLongMatrix[row.index, long.index] - other[row.index, long.index]).toLong()
            return newLongMatrix
        }
        throw Exception("Matrix dimensions do not match, cannot subtract.")
    }
    operator fun minusAssign(other: Long) {
        for (row in this.internalRows.withIndex())
            for (long in row.value.withIndex())
                this[row.index, long.index] = long.value - other
    }
    operator fun minusAssign(other: LongMatrix) {
        if (this dimensionsEqual other) {
            for (row in this.internalRows.withIndex())
                for (long in row.value.withIndex())
                    this[row.index, long.index] = this[row.index, long.index] - other[row.index, long.index]
            return
        }
        throw Exception("Matrix dimensions do not match, cannot subtract.")
    }

    operator fun times(other: Long): LongMatrix {
        val newLongMatrix = LongMatrix(this.rows, this.columns)
        for (row in this.internalRows.withIndex())
            for (long in row.value.withIndex())
                newLongMatrix[row.index, long.index] = this[row.index, long.index] * other
        return newLongMatrix
    }
    operator fun times(other: LongMatrix): LongMatrix {
        if (this.columns == other.rows) {
            val newLongMatrix = LongMatrix(this.rows, other.columns)
            var currentRow: LongArray
            for (rowIndex in 0 until this.rows) {
                currentRow = this.getRow(rowIndex)
                for (columnIndex in 0 until other.columns)
                    newLongMatrix[rowIndex, columnIndex] = currentRow dot other.getColumn(columnIndex)
            }
            return newLongMatrix

        }
        throw Exception("Matrix dimensions do not match for multiplication, cannot multiply together.")
    }
    operator fun timesAssign(other: Long) {
        for (row in this.internalRows.withIndex())
            for (long in row.value.withIndex())
                this[row.index, long.index] = long.value * other
    }

    operator fun div(other: Long): LongMatrix {
        val newLongMatrix = LongMatrix(this.rows, this.columns)
        for (row in this.internalRows.withIndex())
            for (long in row.value.withIndex())
                newLongMatrix[row.index, long.index] = this[row.index, long.index] / other
        return newLongMatrix
    }
    operator fun divAssign(other: Long) {
        for (row in this.internalRows.withIndex())
            for (long in row.value.withIndex())
                this[row.index, long.index] = long.value / other
    }

    infix fun dimensionsEqual(other: LongMatrix): Boolean = this.rows == other.rows && this.columns == other.columns
    fun transposed(): LongMatrix = LongMatrix(this.columns, this.rows) { this.getColumn(it) }
    fun getRow(rowIndex: Int) = this[rowIndex]
    fun getColumn(columnIndex: Int): LongArray = this.internalRows.map { it[columnIndex] }.toLongArray()
    fun copy(): LongMatrix = LongMatrix(this.rows, this.columns) { this.getRow(it) }
    override fun toString(): String = this.internalRows.joinToString("\n") { it.joinToString(prefix="[", postfix="]") }

    infix fun LongArray.dot(other: LongArray): Long =
        if (this.size == other.size)
            (this zip other).longSumBy {longs -> (longs.first * longs.second) }
        else
            throw Exception("Both arrays must be same length to dot product them")

    private inline fun <T> Iterable<T>.longSumBy(selector: (T) -> Long): Long {
        var sum: Long = 0
        for (element in this) {
            sum += selector(element)
        }
        return sum
    }
}


fun main() {
    val mat1 = LongMatrix(
        arrayOf(
            longArrayOf(0, 3, 5),
            longArrayOf(5, 5, 2)
        )
    )

    val mat2 = LongMatrix(
        arrayOf(
            longArrayOf(3, 4),
            longArrayOf(3, -2),
            longArrayOf(4, -2)
        )
    )

    println(mat1)
    println()
    println(mat2)
    println()
    println(mat1*mat2)
}