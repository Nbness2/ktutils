package MatrixUtil

class ByteMatrix(val rows: Int, val columns: Int, init: (Int) -> ByteArray={ ByteArray(columns) { 0 } }) {
    private val internalRows = Array(rows) {
        val arrayToAdd = init(it)
        if (arrayToAdd.size != columns) throw IndexOutOfBoundsException("initialized arrays cannot be bigger than $columns")
        arrayToAdd.copyOf()
    }

    constructor(init: Array<ByteArray>): this(init.size, init.first().size, { init[it].copyOf() } )

    operator fun get(rowIndex: Int): ByteArray = this.internalRows[rowIndex].copyOf()
    operator fun get(rowIndex: Int, columnIndex: Int): Byte = this.internalRows[rowIndex][columnIndex]
    operator fun set(rowIndex: Int, other: ByteArray) { this.internalRows[rowIndex] = other.copyOf() }
    operator fun set(rowIndex: Int, columnIndex: Int, other: Byte) { this.internalRows[rowIndex][columnIndex] = other }

    operator fun plus(other: Byte): ByteMatrix {
        val newByteMatrix = ByteMatrix(this.rows, this.columns)
        for (row in this.internalRows.withIndex())
            for (byte in row.value.withIndex())
                newByteMatrix[row.index, byte.index] = (this[row.index, byte.index] + other).toByte()
        return newByteMatrix
    }
    operator fun plus(other: ByteMatrix): ByteMatrix {
        if (this dimensionsEqual other) {
            val newByteMatrix = this.copy()
            for (row in this.internalRows.withIndex())
                for (byte in row.value.withIndex())
                    newByteMatrix[row.index, byte.index] = (newByteMatrix[row.index, byte.index] + other[row.index, byte.index]).toByte()
            return newByteMatrix
        }
        throw Exception("Matrix dimensions do not match, cannot pls together.")
    }
    operator fun plusAssign(other: Byte) {
        for (row in this.internalRows.withIndex())
            for (byte in row.value.withIndex())
                this[row.index, byte.index] = (byte.value + other).toByte()
    }
    operator fun plusAssign(other: ByteMatrix) {
        if (this dimensionsEqual other) {
            for (row in this.internalRows.withIndex())
                for (byte in row.value.withIndex())
                    this[row.index, byte.index] = (this[row.index, byte.index] + other[row.index, byte.index]).toByte()
            return
        }
        throw Exception("Matrix dimensions do not match, cannot pls together.")
    }

    operator fun minus(other: Byte): ByteMatrix {
        val newByteMatrix = ByteMatrix(this.rows, this.columns)
        for (row in this.internalRows.withIndex())
            for (byte in row.value.withIndex())
                newByteMatrix[row.index, byte.index] = (this[row.index, byte.index] - other).toByte()
        return newByteMatrix
    }
    operator fun minus(other: ByteMatrix): ByteMatrix {
        if (this dimensionsEqual other) {
            val newByteMatrix = this.copy()
            for (row in this.internalRows.withIndex())
                for (byte in row.value.withIndex())
                    newByteMatrix[row.index, byte.index] = (newByteMatrix[row.index, byte.index] - other[row.index, byte.index]).toByte()
            return newByteMatrix
        }
        throw Exception("Matrix dimensions do not match, cannot subtract.")
    }
    operator fun minusAssign(other: Byte) {
        for (row in this.internalRows.withIndex())
            for (byte in row.value.withIndex())
                this[row.index, byte.index] = (byte.value - other).toByte()
    }
    operator fun minusAssign(other: ByteMatrix) {
        if (this dimensionsEqual other) {
            for (row in this.internalRows.withIndex())
                for (byte in row.value.withIndex())
                    this[row.index, byte.index] = (this[row.index, byte.index] - other[row.index, byte.index]).toByte()
            return
        }
        throw Exception("Matrix dimensions do not match, cannot subtract.")
    }

    operator fun times(other: Byte): ByteMatrix {
        val newByteMatrix = ByteMatrix(this.rows, this.columns)
        for (row in this.internalRows.withIndex())
            for (byte in row.value.withIndex())
                newByteMatrix[row.index, byte.index] = (this[row.index, byte.index] * other).toByte()
        return newByteMatrix
    }
    operator fun times(other: ByteMatrix): ByteMatrix {
        if (this.columns == other.rows) {
            val newByteMatrix = ByteMatrix(this.rows, other.columns)
            var currentRow: ByteArray
            for (rowIndex in 0 until this.rows) {
                currentRow = this.getRow(rowIndex)
                for (columnIndex in 0 until other.columns)
                    newByteMatrix[rowIndex, columnIndex] = currentRow dot other.getColumn(columnIndex)
            }
            return newByteMatrix

        }
        throw Exception("Matrix dimensions do not match for multiplication, cannot multiply together.")
    }
    operator fun timesAssign(other: Byte) {
        for (row in this.internalRows.withIndex())
            for (byte in row.value.withIndex())
                this[row.index, byte.index] = (byte.value * other).toByte()
    }

    operator fun div(other: Byte): ByteMatrix {
        val newByteMatrix = ByteMatrix(this.rows, this.columns)
        for (row in this.internalRows.withIndex())
            for (byte in row.value.withIndex())
                newByteMatrix[row.index, byte.index] = (this[row.index, byte.index] / other).toByte()
        return newByteMatrix
    }
    operator fun divAssign(other: Byte) {
        for (row in this.internalRows.withIndex())
            for (byte in row.value.withIndex())
                this[row.index, byte.index] = (byte.value / other).toByte()
    }

    infix fun dimensionsEqual(other: ByteMatrix): Boolean = this.rows == other.rows && this.columns == other.columns
    fun transposed(): ByteMatrix = ByteMatrix(this.columns, this.rows) { this.getColumn(it) }
    fun getRow(rowIndex: Int) = this[rowIndex]
    fun getColumn(columnIndex: Int): ByteArray = this.internalRows.map { it[columnIndex] }.toByteArray()
    fun copy(): ByteMatrix = ByteMatrix(this.rows, this.columns) { this.getRow(it) }
    override fun toString(): String = this.internalRows.joinToString("\n") { it.joinToString(prefix="[", postfix="]") }

    infix fun ByteArray.dot(other: ByteArray): Byte =
        if (this.size == other.size)
            (this zip other).sumBy {bytes -> (bytes.first * bytes.second) }.toByte()
        else
            throw Exception("Both arrays must be same length to dot product them")
}


fun main() {
    val mat1 = ByteMatrix(
        arrayOf(
            byteArrayOf(0, 3, 5),
            byteArrayOf(50, 5, 2)
        )
    )

    val mat2 = ByteMatrix(
        arrayOf(
            byteArrayOf(3, 4),
            byteArrayOf(3, -2),
            byteArrayOf(4, -2)
        )
    )

    println(mat1)
    println()
    println(mat2)
    println()
    println(mat1*mat2)
}