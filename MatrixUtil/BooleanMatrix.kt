package MatrixUtil

import RandomUtil.randomBooleanArray

class BooleanMatrix(private val rows: Int, private val columns: Int, init: (Int) -> BooleanArray={ BooleanArray(columns) {false} }) {
    private val internalRows = Array(rows) {
        val arrayToAdd = init(it)
        if (arrayToAdd.size != columns) throw IndexOutOfBoundsException("initialized arrays cannot be bigger than $columns")
        arrayToAdd.copyOf()
    }

    constructor(init: Array<BooleanArray>): this(init.size, init.first().size, { init[it].copyOf() } )

    operator fun get(rowIndex: Int): BooleanArray = this.internalRows[rowIndex].copyOf()
    operator fun get(rowIndex: Int, columnIndex: Int): Boolean = this.internalRows[rowIndex][columnIndex]
    operator fun set(rowIndex: Int, other: BooleanArray) { this.internalRows[rowIndex] = other.copyOf() }
    operator fun set(rowIndex: Int, columnIndex: Int, other: Boolean) { this.internalRows[rowIndex][columnIndex] = other }

    /**
     * [invert] inverts all values inside of the matrix in place.
     */
    fun invert() {
        for (array in this.internalRows)
            for (item in array.withIndex())
                array[item.index] = !item.value
    }

    /**
     * [inverted] returns a BooleanMatrix equal to this.[invert]
     */
    fun inverted(): BooleanMatrix = BooleanMatrix(this.rows, this.columns) { !this[it] }

    fun transposed(): BooleanMatrix = BooleanMatrix(this.columns, this.rows) { this.getColumn(it) }
    fun getRow(rowIndex: Int) = this[rowIndex]
    fun getColumn(columnIndex: Int): BooleanArray = this.internalRows.map { it[columnIndex] }.toBooleanArray()
    override fun toString(): String = this.internalRows.joinToString("\n") { it.joinToString(prefix="[", postfix="]") }

    private operator fun BooleanArray.not(): BooleanArray = BooleanArray(this.size) { !this[it] }
}

fun main() {
    val a = BooleanMatrix(4, 8) { randomBooleanArray(8) }
    println(a)
    println()
    println(a.transposed())
}