package MatrixUtil

class FloatMatrix(val rows: Int, val columns: Int, init: (Int) -> FloatArray={ FloatArray(columns) { 0f } }) {
    private val internalRows = Array(rows) {
        val arrayToAdd = init(it)
        if (arrayToAdd.size != columns) throw IndexOutOfBoundsException("initialized arrays cannot be bigger than $columns")
        arrayToAdd.copyOf()
    }

    constructor(init: Array<FloatArray>): this(init.size, init.first().size, { init[it].copyOf() } )

    operator fun get(rowIndex: Int): FloatArray = this.internalRows[rowIndex].copyOf()
    operator fun get(rowIndex: Int, columnIndex: Int): Float = this.internalRows[rowIndex][columnIndex]
    operator fun set(rowIndex: Int, other: FloatArray) { this.internalRows[rowIndex] = other.copyOf() }
    operator fun set(rowIndex: Int, columnIndex: Int, other: Float) { this.internalRows[rowIndex][columnIndex] = other }

    operator fun plus(other: Float): FloatMatrix {
        val newFloatMatrix = FloatMatrix(this.rows, this.columns)
        for (row in this.internalRows.withIndex())
            for (float in row.value.withIndex())
                newFloatMatrix[row.index, float.index] = this[row.index, float.index] + other
        return newFloatMatrix
    }
    operator fun plus(other: FloatMatrix): FloatMatrix {
        if (this dimensionsEqual other) {
            val newFloatMatrix = this.copy()
            for (row in this.internalRows.withIndex())
                for (float in row.value.withIndex())
                    newFloatMatrix[row.index, float.index] = newFloatMatrix[row.index, float.index] + other[row.index, float.index]
            return newFloatMatrix
        }
        throw Exception("Matrix dimensions do not match, cannot pls together.")
    }
    operator fun plusAssign(other: Float) {
        for (row in this.internalRows.withIndex())
            for (float in row.value.withIndex())
                this[row.index, float.index] = float.value + other
    }
    operator fun plusAssign(other: FloatMatrix) {
        if (this dimensionsEqual other) {
            for (row in this.internalRows.withIndex())
                for (float in row.value.withIndex())
                    this[row.index, float.index] = this[row.index, float.index] + other[row.index, float.index]
            return
        }
        throw Exception("Matrix dimensions do not match, cannot pls together.")
    }

    operator fun minus(other: Float): FloatMatrix {
        val newFloatMatrix = FloatMatrix(this.rows, this.columns)
        for (row in this.internalRows.withIndex())
            for (float in row.value.withIndex())
                newFloatMatrix[row.index, float.index] = this[row.index, float.index] - other
        return newFloatMatrix
    }
    operator fun minus(other: FloatMatrix): FloatMatrix {
        if (this dimensionsEqual other) {
            val newFloatMatrix = this.copy()
            for (row in this.internalRows.withIndex())
                for (float in row.value.withIndex())
                    newFloatMatrix[row.index, float.index] = newFloatMatrix[row.index, float.index] - other[row.index, float.index]
            return newFloatMatrix
        }
        throw Exception("Matrix dimensions do not match, cannot subtract.")
    }
    operator fun minusAssign(other: Float) {
        for (row in this.internalRows.withIndex())
            for (float in row.value.withIndex())
                this[row.index, float.index] = float.value - other
    }
    operator fun minusAssign(other: FloatMatrix) {
        if (this dimensionsEqual other) {
            for (row in this.internalRows.withIndex())
                for (float in row.value.withIndex())
                    this[row.index, float.index] = this[row.index, float.index] - other[row.index, float.index]
            return
        }
        throw Exception("Matrix dimensions do not match, cannot subtract.")
    }

    operator fun times(other: Float): FloatMatrix {
        val newFloatMatrix = FloatMatrix(this.rows, this.columns)
        for (row in this.internalRows.withIndex())
            for (float in row.value.withIndex())
                newFloatMatrix[row.index, float.index] = this[row.index, float.index] * other
        return newFloatMatrix
    }
    operator fun times(other: FloatMatrix): FloatMatrix {
        if (this.columns == other.rows) {
            val newFloatMatrix = FloatMatrix(this.rows, other.columns)
            var currentRow: FloatArray
            for (rowIndex in 0 until this.rows) {
                currentRow = this.getRow(rowIndex)
                for (columnIndex in 0 until other.columns)
                    newFloatMatrix[rowIndex, columnIndex] = currentRow dot other.getColumn(columnIndex)
            }
            return newFloatMatrix

        }
        throw Exception("Matrix dimensions do not match for multiplication, cannot multiply together.")
    }
    operator fun timesAssign(other: Float) {
        for (row in this.internalRows.withIndex())
            for (float in row.value.withIndex())
                this[row.index, float.index] = float.value * other
    }

    operator fun div(other: Float): FloatMatrix {
        val newFloatMatrix = FloatMatrix(this.rows, this.columns)
        for (row in this.internalRows.withIndex())
            for (float in row.value.withIndex())
                newFloatMatrix[row.index, float.index] = this[row.index, float.index] / other
        return newFloatMatrix
    }
    operator fun divAssign(other: Float) {
        for (row in this.internalRows.withIndex())
            for (float in row.value.withIndex())
                this[row.index, float.index] = float.value / other
    }

    infix fun dimensionsEqual(other: FloatMatrix): Boolean = this.rows == other.rows && this.columns == other.columns
    fun transposed(): FloatMatrix = FloatMatrix(this.columns, this.rows) { this.getColumn(it) }
    fun getRow(rowIndex: Int) = this[rowIndex]
    fun getColumn(columnIndex: Int): FloatArray = this.internalRows.map { it[columnIndex] }.toFloatArray()
    fun copy(): FloatMatrix = FloatMatrix(this.rows, this.columns) { this.getRow(it) }
    override fun toString(): String = this.internalRows.joinToString("\n") { it.joinToString(prefix="[", postfix="]") }

    infix fun FloatArray.dot(other: FloatArray): Float =
        if (this.size == other.size)
            (this zip other).sumByFloat {floats -> (floats.first * floats.second) }
        else
            throw Exception("Both arrays must be same length to dot product them")

    private inline fun <T> Iterable<T>.sumByFloat(selector: (T) -> Float): Float {
        var sum: Double = 0.0
        for (element in this) {
            sum += selector(element)
        }
        return sum.toFloat()
    }
}


fun main() {
    val mat1 = FloatMatrix(
        arrayOf(
            floatArrayOf(0f, 3f, 5f),
            floatArrayOf(5f, 5f, 2f)
        )
    )

    val mat2 = FloatMatrix(
        arrayOf(
            floatArrayOf(3f, 4f),
            floatArrayOf(3f, -2f),
            floatArrayOf(4f, -2f)
        )
    )

    println(mat1)
    println()
    println(mat2)
    println()
    println(mat1*mat2)
}