package MatrixUtil

class DoubleMatrix(val rows: Int, val columns: Int, init: (Int) -> DoubleArray={ DoubleArray(columns) { 0.0 } }) {
    private val internalRows = Array(rows) {
        val arrayToAdd = init(it)
        if (arrayToAdd.size != columns) throw IndexOutOfBoundsException("initialized arrays cannot be bigger than $columns")
        arrayToAdd.copyOf()
    }

    constructor(init: Array<DoubleArray>): this(init.size, init.first().size, { init[it].copyOf() } )

    operator fun get(rowIndex: Int): DoubleArray = this.internalRows[rowIndex].copyOf()
    operator fun get(rowIndex: Int, columnIndex: Int): Double = this.internalRows[rowIndex][columnIndex]
    operator fun set(rowIndex: Int, other: DoubleArray) { this.internalRows[rowIndex] = other.copyOf() }
    operator fun set(rowIndex: Int, columnIndex: Int, other: Double) { this.internalRows[rowIndex][columnIndex] = other }

    operator fun plus(other: Double): DoubleMatrix {
        val newDoubleMatrix = DoubleMatrix(this.rows, this.columns)
        for (row in this.internalRows.withIndex())
            for (float in row.value.withIndex())
                newDoubleMatrix[row.index, float.index] = this[row.index, float.index] + other
        return newDoubleMatrix
    }
    operator fun plus(other: DoubleMatrix): DoubleMatrix {
        if (this dimensionsEqual other) {
            val newDoubleMatrix = this.copy()
            for (row in this.internalRows.withIndex())
                for (float in row.value.withIndex())
                    newDoubleMatrix[row.index, float.index] = newDoubleMatrix[row.index, float.index] + other[row.index, float.index]
            return newDoubleMatrix
        }
        throw Exception("Matrix dimensions do not match, cannot pls together.")
    }
    operator fun plusAssign(other: Double) {
        for (row in this.internalRows.withIndex())
            for (float in row.value.withIndex())
                this[row.index, float.index] = float.value + other
    }
    operator fun plusAssign(other: DoubleMatrix) {
        if (this dimensionsEqual other) {
            for (row in this.internalRows.withIndex())
                for (float in row.value.withIndex())
                    this[row.index, float.index] = this[row.index, float.index] + other[row.index, float.index]
            return
        }
        throw Exception("Matrix dimensions do not match, cannot pls together.")
    }

    operator fun minus(other: Double): DoubleMatrix {
        val newDoubleMatrix = DoubleMatrix(this.rows, this.columns)
        for (row in this.internalRows.withIndex())
            for (float in row.value.withIndex())
                newDoubleMatrix[row.index, float.index] = this[row.index, float.index] - other
        return newDoubleMatrix
    }
    operator fun minus(other: DoubleMatrix): DoubleMatrix {
        if (this dimensionsEqual other) {
            val newDoubleMatrix = this.copy()
            for (row in this.internalRows.withIndex())
                for (float in row.value.withIndex())
                    newDoubleMatrix[row.index, float.index] = newDoubleMatrix[row.index, float.index] - other[row.index, float.index]
            return newDoubleMatrix
        }
        throw Exception("Matrix dimensions do not match, cannot subtract.")
    }
    operator fun minusAssign(other: Double) {
        for (row in this.internalRows.withIndex())
            for (float in row.value.withIndex())
                this[row.index, float.index] = float.value - other
    }
    operator fun minusAssign(other: DoubleMatrix) {
        if (this dimensionsEqual other) {
            for (row in this.internalRows.withIndex())
                for (float in row.value.withIndex())
                    this[row.index, float.index] = this[row.index, float.index] - other[row.index, float.index]
            return
        }
        throw Exception("Matrix dimensions do not match, cannot subtract.")
    }

    operator fun times(other: Double): DoubleMatrix {
        val newDoubleMatrix = DoubleMatrix(this.rows, this.columns)
        for (row in this.internalRows.withIndex())
            for (float in row.value.withIndex())
                newDoubleMatrix[row.index, float.index] = this[row.index, float.index] * other
        return newDoubleMatrix
    }
    operator fun times(other: DoubleMatrix): DoubleMatrix {
        if (this.columns == other.rows) {
            val newDoubleMatrix = DoubleMatrix(this.rows, other.columns)
            var currentRow: DoubleArray
            for (rowIndex in 0 until this.rows) {
                currentRow = this.getRow(rowIndex)
                for (columnIndex in 0 until other.columns)
                    newDoubleMatrix[rowIndex, columnIndex] = currentRow dot other.getColumn(columnIndex)
            }
            return newDoubleMatrix

        }
        throw Exception("Matrix dimensions do not match for multiplication, cannot multiply together.")
    }
    operator fun timesAssign(other: Double) {
        for (row in this.internalRows.withIndex())
            for (float in row.value.withIndex())
                this[row.index, float.index] = float.value * other
    }

    operator fun div(other: Double): DoubleMatrix {
        val newDoubleMatrix = DoubleMatrix(this.rows, this.columns)
        for (row in this.internalRows.withIndex())
            for (float in row.value.withIndex())
                newDoubleMatrix[row.index, float.index] = this[row.index, float.index] / other
        return newDoubleMatrix
    }
    operator fun divAssign(other: Double) {
        for (row in this.internalRows.withIndex())
            for (float in row.value.withIndex())
                this[row.index, float.index] = float.value / other
    }

    infix fun dimensionsEqual(other: DoubleMatrix): Boolean = this.rows == other.rows && this.columns == other.columns
    fun transposed(): DoubleMatrix = DoubleMatrix(this.columns, this.rows) { this.getColumn(it) }
    fun getRow(rowIndex: Int) = this[rowIndex]
    fun getColumn(columnIndex: Int): DoubleArray = this.internalRows.map { it[columnIndex] }.toDoubleArray()
    fun copy(): DoubleMatrix = DoubleMatrix(this.rows, this.columns) { this.getRow(it) }
    override fun toString(): String = this.internalRows.joinToString("\n") { it.joinToString(prefix="[", postfix="]") }

    private infix fun DoubleArray.dot(other: DoubleArray): Double =
        if (this.size == other.size)
            (this zip other).sumByDouble {floats -> (floats.first * floats.second) }
        else
            throw Exception("Both arrays must be same length to dot product them")
}


fun main() {
    val mat1 = DoubleMatrix(
        arrayOf(
            doubleArrayOf(0.0, 3.0, 0.0),
            doubleArrayOf(0.0, 0.0, 0.0)
        )
    )

    val mat2 = DoubleMatrix(
        arrayOf(
            doubleArrayOf(3.0, 4.0),
            doubleArrayOf(3.0, -2.0),
            doubleArrayOf(4.0, -2.0)
        )
    )

    println(mat1)
    println()
    println(mat2)
    println()
    println(mat1*mat2)
}