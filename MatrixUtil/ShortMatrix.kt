package MatrixUtil

class ShortMatrix(val rows: Int, val columns: Int, init: (Int) -> ShortArray={ ShortArray(columns) { 0 } }) {
    private val internalRows = Array(rows) {
        val arrayToAdd = init(it)
        if (arrayToAdd.size != columns) throw IndexOutOfBoundsException("initialized arrays cannot be bigger than $columns")
        arrayToAdd.copyOf()
    }

    constructor(init: Array<ShortArray>): this(init.size, init.first().size, { init[it].copyOf() } )

    operator fun get(rowIndex: Int): ShortArray = this.internalRows[rowIndex].copyOf()
    operator fun get(rowIndex: Int, columnIndex: Int): Short = this.internalRows[rowIndex][columnIndex]
    operator fun set(rowIndex: Int, other: ShortArray) { this.internalRows[rowIndex] = other.copyOf() }
    operator fun set(rowIndex: Int, columnIndex: Int, other: Short) { this.internalRows[rowIndex][columnIndex] = other }

    operator fun plus(other: Short): ShortMatrix {
        val newShortMatrix = ShortMatrix(this.rows, this.columns)
        for (row in this.internalRows.withIndex())
            for (short in row.value.withIndex())
                newShortMatrix[row.index, short.index] = (this[row.index, short.index] + other).toShort()
        return newShortMatrix
    }
    operator fun plus(other: ShortMatrix): ShortMatrix {
        if (this dimensionsEqual other) {
            val newShortMatrix = this.copy()
            for (row in this.internalRows.withIndex())
                for (short in row.value.withIndex())
                    newShortMatrix[row.index, short.index] = (newShortMatrix[row.index, short.index] + other[row.index, short.index]).toShort()
            return newShortMatrix
        }
        throw Exception("Matrix dimensions do not match, cannot pls together.")
    }
    operator fun plusAssign(other: Short) {
        for (row in this.internalRows.withIndex())
            for (short in row.value.withIndex())
                this[row.index, short.index] = (short.value + other).toShort()
    }
    operator fun plusAssign(other: ShortMatrix) {
        if (this dimensionsEqual other) {
            for (row in this.internalRows.withIndex())
                for (short in row.value.withIndex())
                    this[row.index, short.index] = (this[row.index, short.index] + other[row.index, short.index]).toShort()
            return
        }
        throw Exception("Matrix dimensions do not match, cannot pls together.")
    }

    operator fun minus(other: Short): ShortMatrix {
        val newShortMatrix = ShortMatrix(this.rows, this.columns)
        for (row in this.internalRows.withIndex())
            for (short in row.value.withIndex())
                newShortMatrix[row.index, short.index] = (this[row.index, short.index] - other).toShort()
        return newShortMatrix
    }
    operator fun minus(other: ShortMatrix): ShortMatrix {
        if (this dimensionsEqual other) {
            val newShortMatrix = this.copy()
            for (row in this.internalRows.withIndex())
                for (short in row.value.withIndex())
                    newShortMatrix[row.index, short.index] = (newShortMatrix[row.index, short.index] - other[row.index, short.index]).toShort()
            return newShortMatrix
        }
        throw Exception("Matrix dimensions do not match, cannot subtract.")
    }
    operator fun minusAssign(other: Short) {
        for (row in this.internalRows.withIndex())
            for (short in row.value.withIndex())
                this[row.index, short.index] = (short.value - other).toShort()
    }
    operator fun minusAssign(other: ShortMatrix) {
        if (this dimensionsEqual other) {
            for (row in this.internalRows.withIndex())
                for (short in row.value.withIndex())
                    this[row.index, short.index] = (this[row.index, short.index] - other[row.index, short.index]).toShort()
            return
        }
        throw Exception("Matrix dimensions do not match, cannot subtract.")
    }

    operator fun times(other: Short): ShortMatrix {
        val newShortMatrix = ShortMatrix(this.rows, this.columns)
        for (row in this.internalRows.withIndex())
            for (short in row.value.withIndex())
                newShortMatrix[row.index, short.index] = (this[row.index, short.index] * other).toShort()
        return newShortMatrix
    }
    operator fun times(other: ShortMatrix): ShortMatrix {
        if (this.columns == other.rows) {
            val newShortMatrix = ShortMatrix(this.rows, other.columns)
            var currentRow: ShortArray
            for (rowIndex in 0 until this.rows) {
                currentRow = this.getRow(rowIndex)
                for (columnIndex in 0 until other.columns)
                    newShortMatrix[rowIndex, columnIndex] = currentRow dot other.getColumn(columnIndex)
            }
            return newShortMatrix

        }
        throw Exception("Matrix dimensions do not match for multiplication, cannot multiply together.")
    }
    operator fun timesAssign(other: Short) {
        for (row in this.internalRows.withIndex())
            for (short in row.value.withIndex())
                this[row.index, short.index] = (short.value * other).toShort()
    }

    operator fun div(other: Short): ShortMatrix {
        val newShortMatrix = ShortMatrix(this.rows, this.columns)
        for (row in this.internalRows.withIndex())
            for (short in row.value.withIndex())
                newShortMatrix[row.index, short.index] = (this[row.index, short.index] / other).toShort()
        return newShortMatrix
    }
    operator fun divAssign(other: Short) {
        for (row in this.internalRows.withIndex())
            for (short in row.value.withIndex())
                this[row.index, short.index] = (short.value / other).toShort()
    }

    infix fun dimensionsEqual(other: ShortMatrix): Boolean = this.rows == other.rows && this.columns == other.columns
    fun transposed(): ShortMatrix = ShortMatrix(this.columns, this.rows) { this.getColumn(it) }
    fun getRow(rowIndex: Int) = this[rowIndex]
    fun getColumn(columnIndex: Int): ShortArray = this.internalRows.map { it[columnIndex] }.toShortArray()
    fun copy(): ShortMatrix = ShortMatrix(this.rows, this.columns) { this.getRow(it) }
    override fun toString(): String = this.internalRows.joinToString("\n") { it.joinToString(prefix="[", postfix="]") }

    infix fun ShortArray.dot(other: ShortArray): Short =
        if (this.size == other.size)
            (this zip other).sumBy {shorts -> (shorts.first * shorts.second) }.toShort()
        else
            throw Exception("Both arrays must be same length to dot product them")
}


fun main() {
    val mat1 = ShortMatrix(
        arrayOf(
            shortArrayOf(0, 3, 5),
            shortArrayOf(50, 5, 2)
        )
    )

    val mat2 = ShortMatrix(
        arrayOf(
            shortArrayOf(3, 4),
            shortArrayOf(3, -2),
            shortArrayOf(4, -2)
        )
    )

    println(mat1)
    println()
    println(mat2)
    println()
    println(mat1*mat2)
}