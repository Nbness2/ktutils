package NumberUtil

/**
 * @Author: nbness2
 */

/**
 * Generic [Number.toByteArray].
 * Returns the bytes that would make up the number if you looked at it in memory.
 * This performs worse but has the same output as the inlined primitive [toByteArray] functions.
 * Can change byte ordering with [bigEndian]
 */
fun Number.toByteArray(bigEndian: Boolean=true): ByteArray {
    val proxyLong: Long = when (this) {
        is Byte, is Short, is Int, is Long ->
            this.toLong()

        is Float ->
            this.toRawBits().toLong()

        is Double ->
            this.toRawBits()

        else ->
            throw NotImplementedError("${this::class.qualifiedName} not implemented for Number.toByteArray()")
    }

    val size = when (this) {
        is Byte ->
            Byte.SIZE_BYTES

        is Short ->
            Short.SIZE_BYTES

        is Float ->
            Int.SIZE_BYTES

        is Int ->
            Int.SIZE_BYTES

        is Double ->
            Long.SIZE_BYTES

        is Long ->
            Long.SIZE_BYTES

        else ->
            throw NotImplementedError("${this::class.qualifiedName} not implemented for Number.toByteArray()")
    }

    var power = if (bigEndian) size else -1
    val direction: Int = if (bigEndian) -1 else 1
    return ByteArray(size) {
        power += direction
        (proxyLong ushr 8*power and 0xFF).toByte()
    }
}

fun Byte.toByteArray(bigEndian: Boolean=true): ByteArray = ByteArray(1) { this }
fun Short.toByteArray(bigEndian: Boolean=true): ByteArray {
    val proxyInt = this.toInt()
    if (bigEndian)
        return byteArrayOf(
            (proxyInt ushr 8 and 0xFF).toByte(),
            (proxyInt and 0xFF).toByte()
        )

    return byteArrayOf(
        (proxyInt and 0xFF).toByte(),
        (proxyInt ushr 8 and 0xFF).toByte()
    )
}
fun Int.toByteArray(bigEndian: Boolean=true): ByteArray {
    if (bigEndian)
        return byteArrayOf(
            (this ushr 24 and 0xFF).toByte(),
            (this ushr 16 and 0xFF).toByte(),
            (this ushr 8 and 0xFF).toByte(),
            (this and 0xFF).toByte()
        )

    return byteArrayOf(
        (this and 0xFF).toByte(),
        (this ushr 8 and 0xFF).toByte(),
        (this ushr 16 and 0xFF).toByte(),
        (this ushr 24 and 0xFF).toByte()
    )
}
fun Long.toByteArray(bigEndian: Boolean=true): ByteArray {
    if (bigEndian)
        return byteArrayOf(
            (this ushr 56 and 0xFF).toByte(),
            (this ushr 48 and 0xFF).toByte(),
            (this ushr 40 and 0xFF).toByte(),
            (this ushr 32 and 0xFF).toByte(),
            (this ushr 24 and 0xFF).toByte(),
            (this ushr 16 and 0xFF).toByte(),
            (this ushr 8 and 0xFF).toByte(),
            (this and 0xFF).toByte()
        )

    return byteArrayOf(
        (this and 0xFF).toByte(),
        (this ushr 8 and 0xFF).toByte(),
        (this ushr 16 and 0xFF).toByte(),
        (this ushr 24 and 0xFF).toByte(),
        (this ushr 32 and 0xFF).toByte(),
        (this ushr 40 and 0xFF).toByte(),
        (this ushr 48 and 0xFF).toByte(),
        (this ushr 56 and 0xFF).toByte()
    )
}
fun Float.toByteArray(bigEndian: Boolean=true): ByteArray = this.toRawBits().toByteArray(bigEndian=bigEndian)
fun Double.toByteArray(bigEndian: Boolean=true): ByteArray = this.toRawBits().toByteArray(bigEndian=bigEndian)
