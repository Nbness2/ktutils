package NumberUtil

/**
 * @Author: nbness2
 */

/**
 * Check if the [Number] is even or odd by checking the 0-bit
 */
fun Number.isEven(): Boolean = (this.toLong() and 1) == 0L
fun Long.isEven(): Boolean = (this and 1).toInt() == 0
fun Int.isEven(): Boolean = (this and 1) == 0
fun Short.isEven(): Boolean = this.toInt() and 1 == 0
fun Byte.isEven(): Boolean = (this.toInt() and 1) == 0
fun Number.isOdd(): Boolean = (this.toLong() and 1) == 1L
fun Long.isOdd(): Boolean = (this and 1).toInt() == 1
fun Int.isOdd(): Boolean = (this and 1) == 1
fun Short.isOdd(): Boolean = this.toInt() and 1 == 1
fun Byte.isOdd(): Boolean = (this.toInt() and 1) == 1

/**
 * Check if the internal bits that represent these types ([Float] -> [Int] or [Double] -> [Long]) is even or odd.
 */
fun Float.bitsAreEven(): Boolean = this.toRawBits().isEven()
fun Double.bitsAreEven(): Boolean = this.toRawBits().isEven()

/**
 * Check if the truncated (removed decimals) integral ([Float] -> [Int] or [Double] -> [Long]) is even or odd
 */
fun Float.integralIsEven(): Boolean = this.toInt().isEven()
fun Double.integralIsEven(): Boolean = this.toLong().isEven()
