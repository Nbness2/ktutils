package NumberUtil

internal val hexCharset: CharArray = charArrayOf('0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F')
/**
 * @Author: nbness2
 */

/**
 * Generic [Number.toHex].
 * This returns the unsigned hex value of the data type.
 * This performs worse but has the same output as the inlined primitive [toHex] functions.
 * Can change byte order using [bigEndian]
 * Can omit "0x" using [withx]
 */
fun Number.toHex(bigEndian: Boolean=true, withx: Boolean=true): String {

    val proxyLong: Long = when (this) {
        is Byte, is Short, is Int, is Long ->
            this.toLong()

        is Float ->
            this.toRawBits().toLong()

        is Double ->
            this.toRawBits()

        else ->
            throw NotImplementedError("${this::class.qualifiedName} not implemented for Number.toByteArray()")
    }

    val size = when (this) {
        is Byte ->
            Byte.SIZE_BYTES

        is Short ->
            Short.SIZE_BYTES

        is Float ->
            Int.SIZE_BYTES

        is Int ->
            Int.SIZE_BYTES

        is Double ->
            Long.SIZE_BYTES

        is Long ->
            Long.SIZE_BYTES

        else ->
            throw NotImplementedError("${this::class.qualifiedName} not implemented for Number.toByteArray()")
    }

    val hexString = StringBuilder(if (withx) "0x" else "")
    var power = if (bigEndian) size else -1
    val direction: Int = if (bigEndian) -1 else 1
    var currentByte: Int
    repeat(size) {
        power += direction
        currentByte = (proxyLong ushr 8*power and 0xFF).toByte().toInt()
        hexString.append(hexCharset[currentByte ushr 4 and 0xF])
        hexString.append(hexCharset[currentByte and 0xF])
    }
    return hexString.toString()
}

fun Byte.toHex(bigEndian: Boolean=true, withx: Boolean=true): String {
    val proxyInt = this.toInt()
    return (if(withx)"0x" else "") +
            "${hexCharset[proxyInt ushr 4 and 0xF]}${hexCharset[proxyInt and 0xF]}"
}

fun Short.toHex(bigEndian: Boolean=true, withx: Boolean=true): String {
    val proxyInt = this.toInt()
    if (bigEndian)
        return (if(withx)"0x" else "") +
                "${hexCharset[proxyInt ushr 12 and 0xF]}${hexCharset[proxyInt ushr 8 and 0xF]}" +
                "${hexCharset[proxyInt ushr 4 and 0xF]}${hexCharset[proxyInt and 0xF]}"

    return (if(withx)"0x" else "") +
            "${hexCharset[proxyInt ushr 4 and 0xF]}${hexCharset[proxyInt and 0xF]}" +
            "${hexCharset[proxyInt ushr 12 and 0xF]}${hexCharset[proxyInt ushr 8 and 0xF]}"
}

fun Int.toHex(bigEndian: Boolean=true, withx: Boolean=true): String {
    if (bigEndian)
        return (if(withx)"0x" else "") +
                "${hexCharset[this ushr 28 and 0xF]}${hexCharset[this ushr 24 and 0xF]}" +
                "${hexCharset[this ushr 20 and 0xF]}${hexCharset[this ushr 16 and 0xF]}" +
                "${hexCharset[this ushr 12 and 0xF]}${hexCharset[this ushr 8 and 0xF]}" +
                "${hexCharset[this ushr 4 and 0xF]}${hexCharset[this and 0xF]}"

    return (if(withx)"0x" else "") +
            "${hexCharset[this ushr 4 and 0xF]}${hexCharset[this and 0xF]}" +
            "${hexCharset[this ushr 12 and 0xF]}${hexCharset[this ushr 8 and 0xF]}" +
            "${hexCharset[this ushr 20 and 0xF]}${hexCharset[this ushr 16 and 0xF]}" +
            "${hexCharset[this ushr 28 and 0xF]}${hexCharset[this ushr 24 and 0xF]}"
}

/**
 * [proxyInt] is used here because even though longs can be bitshifted, they cannot be used for indexing primitive stdlib arrays.
 * I find it better to convert the long to 2 ints one time over bitshifting masking and then converting to int every time I want to access [hexCharset]
 */
fun Long.toHex(bigEndian: Boolean=true, withx: Boolean=true): String {
    val proxyInt1: Int
    val proxyInt2: Int
    return if (bigEndian) {
        proxyInt1 = (this ushr 32).toInt()
        proxyInt2 = (this and 0xFFFFFFFF).toInt()
        (if(withx)"0x" else "") +
                "${hexCharset[proxyInt1 ushr 28 and 0xF]}${hexCharset[proxyInt1 ushr 24 and 0xF]}" +
                "${hexCharset[proxyInt1 ushr 20 and 0xF]}${hexCharset[proxyInt1 ushr 16 and 0xF]}" +
                "${hexCharset[proxyInt1 ushr 12 and 0xF]}${hexCharset[proxyInt1 ushr 8 and 0xF]}" +
                "${hexCharset[proxyInt1 ushr 4 and 0xF]}${hexCharset[proxyInt1 and 0xF]}" +
                "${hexCharset[proxyInt2 ushr 28 and 0xF]}${hexCharset[proxyInt2 ushr 24 and 0xF]}" +
                "${hexCharset[proxyInt2 ushr 20 and 0xF]}${hexCharset[proxyInt2 ushr 16 and 0xF]}" +
                "${hexCharset[proxyInt2 ushr 12 and 0xF]}${hexCharset[proxyInt2 ushr 8 and 0xF]}" +
                "${hexCharset[proxyInt2 ushr 4 and 0xF]}${hexCharset[proxyInt2 and 0xF]}"
    } else {
        proxyInt1 = (this and 0xFFFFFFFF).toInt()
        proxyInt2 = (this ushr 32).toInt()
        (if(withx)"0x" else "") +
                "${hexCharset[proxyInt1 ushr 4 and 0xF]}${hexCharset[proxyInt1 and 0xF]}" +
                "${hexCharset[proxyInt1 ushr 12 and 0xF]}${hexCharset[proxyInt1 ushr 8 and 0xF]}" +
                "${hexCharset[proxyInt1 ushr 20 and 0xF]}${hexCharset[proxyInt1 ushr 16 and 0xF]}" +
                "${hexCharset[proxyInt1 ushr 28 and 0xF]}${hexCharset[proxyInt1 ushr 24 and 0xF]}" +
                "${hexCharset[proxyInt2 ushr 4 and 0xF]}${hexCharset[proxyInt2 and 0xF]}" +
                "${hexCharset[proxyInt2 ushr 12 and 0xF]}${hexCharset[proxyInt2 ushr 8 and 0xF]}" +
                "${hexCharset[proxyInt2 ushr 20 and 0xF]}${hexCharset[proxyInt2 ushr 16 and 0xF]}" +
                "${hexCharset[proxyInt2 ushr 28 and 0xF]}${hexCharset[proxyInt2 ushr 24 and 0xF]}"
    }
}

fun Float.toHex(bigEndian: Boolean=true, withx: Boolean=true): String = this.toRawBits().toHex(bigEndian=bigEndian, withx=withx)
fun Double.toHex(bigEndian: Boolean=true, withx: Boolean=true): String = this.toRawBits().toHex(bigEndian=bigEndian, withx=withx)
fun ByteArray.toHex(bigEndian: Boolean=true, withx: Boolean=true): String {
    var proxyInt: Int
    return if (bigEndian) {
        this.joinToString(separator = "", prefix = if (withx) "0x" else "") {
            proxyInt = it.toInt()
            "${hexCharset[proxyInt ushr 4 and 0xF]}${hexCharset[proxyInt and 0xF]}"
        }
    } else {
        this.reversedArray().joinToString(separator = "", prefix = if (withx) "0x" else "") {
            proxyInt = it.toInt()
            "${hexCharset[proxyInt ushr 4 and 0xF]}${hexCharset[proxyInt and 0xF]}"
        }
    }
}