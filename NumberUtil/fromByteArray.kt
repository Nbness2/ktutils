package NumberUtil

/**
 * @Author: nbness2
 */

/**
 * This pads the given [ByteArray] to [toSize] size unless it is already at or above that size
 */
private fun ByteArray.padded(toSize: Int): ByteArray {
    if (this.size >= toSize)
        return this
    return ByteArray(this.size-toSize){0} + this
}

/**
 * All the following functions return the respective primitive type constructed from a [ByteArray].
 * There is no generic function because [Number] is abstract and creating a generic [T.fromBytes] requires some ugly
 * looking code so these are all hardcoded to give a smoother experience :)
 */
fun Byte.Companion.fromByteArray(bytes: ByteArray, bigEndian: Boolean=true): Byte {
    if (bytes.isEmpty())
        return 0
    return bytes[0]
}

fun Short.Companion.fromByteArray(bytes: ByteArray, bigEndian: Boolean=true): Short {
    val finalBytes = if (bytes.size >= Short.SIZE_BYTES) bytes else bytes.padded(Short.SIZE_BYTES)
    return if (bigEndian)
        (
                (finalBytes[0].toInt() and 0xFF shl 8) +
                        (finalBytes[1].toInt() and 0xFF)
                ).toShort()
    else
        (
                (finalBytes[1].toInt() and 0xFF shl 8) +
                        (finalBytes[0].toInt() and 0xFF shl 0)
                ).toShort()
}


fun Int.Companion.fromByteArray(bytes: ByteArray, bigEndian: Boolean=true): Int {
    val finalBytes = if (bytes.size >= Int.SIZE_BYTES) bytes else bytes.padded(Int.SIZE_BYTES)
    return if (bigEndian)
        (
                (finalBytes[0].toInt() and 0xFF shl 24) +
                        (finalBytes[1].toInt() and 0xFF shl 16) +
                        (finalBytes[2].toInt() and 0xFF shl 8) +
                        (finalBytes[3].toInt() and 0xFF)
                )
    else
        (
                (finalBytes[3].toInt() and 0xFF shl 24) +
                        (finalBytes[2].toInt() and 0xFF shl 16) +
                        (finalBytes[1].toInt() and 0xFF shl 8) +
                        (finalBytes[0].toInt() and 0xFF)
                )
}

fun Long.Companion.fromByteArray(bytes: ByteArray, bigEndian: Boolean=true): Long {
    val finalBytes = if (bytes.size >= Long.SIZE_BYTES) bytes else bytes.padded(Long.SIZE_BYTES)
    return  if (bigEndian)
        (
                (finalBytes[0].toLong() and 0xFF shl 56) +
                        (finalBytes[1].toLong() and 0xFF shl 48) +
                        (finalBytes[2].toLong() and 0xFF shl 40) +
                        (finalBytes[3].toLong() and 0xFF shl 32) +
                        (finalBytes[4].toLong() and 0xFF shl 24) +
                        (finalBytes[5].toLong() and 0xFF shl 16) +
                        (finalBytes[6].toLong() and 0xFF shl 8) +
                        (finalBytes[7].toLong() and 0xFF)
                )
    else
        (
                (finalBytes[7].toLong() and 0xFF shl 56) +
                        (finalBytes[6].toLong() and 0xFF shl 48) +
                        (finalBytes[5].toLong() and 0xFF shl 40) +
                        (finalBytes[4].toLong() and 0xFF shl 32) +
                        (finalBytes[3].toLong() and 0xFF shl 24) +
                        (finalBytes[2].toLong() and 0xFF shl 16) +
                        (finalBytes[1].toLong() and 0xFF shl 8) +
                        (finalBytes[0].toLong() and 0xFF)
                )
}

fun Float.Companion.fromByteArray(bytes: ByteArray, bigEndian: Boolean=true): Float = Float.fromBits(Int.fromByteArray(bytes, bigEndian))
fun Double.Companion.fromByteArray(bytes: ByteArray, bigEndian: Boolean=true): Double = Double.fromBits(Long.fromByteArray(bytes, bigEndian))
