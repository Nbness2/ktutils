package TextUtil

/**
 * @author: nbness2
 */

/**
 * [TranslationTable] is used as a template of what characters to map to what when translating a string
 */
class TranslationTable(inChars: CharArray, outChars: CharArray) {
    constructor(inChars: String, outChars: String): this(inChars.toCharArray(), outChars.toCharArray())
    private val inChars: CharArray
    private val outChars: CharArray

    init {
        if (inChars.size != outChars.size)
            throw IndexOutOfBoundsException("inChars and outChars must be the same length to create a TranslationTable")
        this.inChars = inChars
        this.outChars = outChars
    }

    /**
     * Gets a [Char] from [outChars] that matches the index for the given [inChar].
     * If [inChar] is not in [inChars], [inChar] will be returned.
     */
    fun getCharFor(inChar: Char): Char {
        if (inChar in inChars)
            return outChars[inChars.indexOf(inChar)]
        return inChar
    }

    /**
     * Translates [inString], calling [getCharFor] for each [Char] in [inString].
     */
    fun translateString(inString: String): String {
        val translatedString = StringBuilder()
        for (character in inString)
            translatedString.append(this.getCharFor(character))
        return translatedString.toString()
    }

    /**
     * Gets a [Char] from [inChars] that matches the index for the given [outChar].
     * If [outChar] is not in [outChars], [outChar] will be returned.
     */
    fun reverseGetCharFor(outChar: Char): Char {
        if (outChar in outChars)
            return inChars[outChars.indexOf(outChar)]
        return outChar
    }

    /**
     * Translates [inString], calling [reverseGetCharFor] for each [Char] in [inString]
     */
    fun reverseTranslateString(inString: String): String {
        val translatedString = StringBuilder()
        for (character in inString)
            translatedString.append(this.reverseGetCharFor(character))
        return translatedString.toString()
    }

    override fun toString(): String = "TranslationTable{ ($inChars) <-> ($outChars) }"
}

/**
 * Translates [this] with the given [TranslationTable] in the given direction.
 */
fun String.translate(translationTable: TranslationTable): String = translationTable.translateString(this)
fun String.reverseTranslate(translationTable: TranslationTable): String = translationTable.reverseTranslateString(this)

fun main() {
    val transTab = TranslationTable("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ", "zyxwvutsrqpnmolkjihgfedcbaZYXWVUTSRQPNMOLKJIHGFEDCBA")
    val transTab2 = TranslationTable("zyxwvutsrqpnmolkjihgfedcbaZYXWVUTSRQPNMOLKJIHGFEDCBA", "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ")
    val demoString = "Jim quickly realized that the beautiful gowns are expensive"
    println(demoString)
    println(demoString.translate(transTab))
    println(demoString.translate(transTab).translate(transTab2))
}