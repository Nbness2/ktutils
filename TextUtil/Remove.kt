package TextUtil

/**
 * @author: nbness2
 */

/**
 * Removes all exact instances of [toRemove] from the string and returns.
 */
fun String.remove(toRemove: String): String {
    if (toRemove.isEmpty()) return this
    if (toRemove.length == 1) return this.remove(toRemove[0])
    val finalString = StringBuilder()
    var slicedString: String
    var slicedIndex = 0
    var sliceMax: Int
    for (character in this.withIndex()) {
        sliceMax = character.index + toRemove.length
        if (sliceMax >= this.length)
            sliceMax = this.length
        slicedString = this.slice(character.index until sliceMax)
        if (slicedString == toRemove)
            slicedIndex = toRemove.length
        if (slicedIndex > 0)
            slicedIndex--
        else
            finalString.append(character.value)
    }
    return finalString.toString()
}

fun String.remove(toRemove: Char): String {
    val finalString = StringBuilder()
    for (character in this)
        if (character != toRemove)
            finalString.append(character)
    return finalString.toString()
}

fun main() {
    val testString = "NOTCOOKIESI love NOTCOOKIEScookies!!NOTCOOKIES! Don't judge NOTCOOKIESme..."
    println(testString.remove("NOTCOOKIES"))
}