package TextUtil

import java.math.BigDecimal
/**
 * @author: nbness2
 */

/**
 * Alphabetic refers to what you would say if the word started with that letter.
 * You would say "An apple" or "A dog", not "An cat" or "A idol"
 */
val ALPHABETIC_VOWELS = charArrayOf(
    'a', 'e', 'i', 'o', 'u',
    'A', 'E', 'I', 'O', 'U'
)

val ALPHABETIC_CONSONANTS = charArrayOf(
    'b', 'c', 'd', 'f', 'g', 'h', 'j', 'k', 'l', 'm', 'n', 'p', 'q', 'r', 's', 't', 'v', 'w', 'x', 'y', 'z',
    'B', 'C', 'D', 'F', 'G', 'H', 'J', 'K', 'L', 'M', 'N', 'P', 'Q', 'R', 'S', 'T', 'V', 'W', 'X', 'Y', 'Z'
)

/**
 * Acronymic refers to what you would say when saying the name of the letter.
 * You would say "An A" or "A 7", not "A I" or "An T"
 */
val ACRONYMIC_VOWELS = charArrayOf(
    'a', 'e', 'f', 'h', 'i', 'l', 'm', 'n', 'o', 'r', 's', 'x',
    'A', 'E', 'F', 'H', 'I', 'L', 'M', 'N', 'O', 'R', 'S', 'X', '8'
)

val ACRONYMIC_CONSONANTS = charArrayOf(
    'b', 'c', 'd', 'g', 'j', 'k', 'p', 'q', 't', 'u', 'v', 'w', 'y', 'z',
    'B', 'C', 'D', 'G', 'J', 'K', 'P', 'Q', 'T', 'U', 'V', 'W', 'Y', 'Z',
    '0', '1', '2', '3', '4', '5', '6', '7', '9'
)

/**
 * Checks if string starts with an alphabetic vowel
 */
fun String.startsWithAlphaVowel(): Boolean =
    if (this.isNotEmpty())
        if (this[0].toLowerCase() == 'y' && this.length > 1)
            // For words like yttrium, ypsilon, yvonne
            this[1] in ALPHABETIC_CONSONANTS
        else
            this[0] in ALPHABETIC_VOWELS
    else
        false

/**
 * Checks if string starts with an alphabetic consonant
 */
fun String.startsWithAlphaConsonant(): Boolean =
    if (this.isNotEmpty())
        if (this[0].toLowerCase() == 'y' && this.length > 1)
            this[1] in ALPHABETIC_VOWELS
        else
            this[0] in ALPHABETIC_CONSONANTS
    else
        false

/**
 * Checks if string starts with an acronymic consonant
 */
fun String.startsWithAcroConsonant(): Boolean =
        if (this.isNotEmpty())
            this[0] in ACRONYMIC_CONSONANTS
        else
            false

/**
 * Checks if string starts with an acronymic vowel
 */
fun String.startsWithAcroVowel(): Boolean =
        if (this.isNotEmpty())
            this[0] in ACRONYMIC_VOWELS
        else
            false

/**
 * Checks if string starts with a vowel.
 * Default: Alphabetic
 */
fun String.startsWithVowel(checkAlphabetic: Boolean=true): Boolean =
        if (checkAlphabetic)
            this.startsWithAlphaVowel()
        else
            this.startsWithAcroVowel()

/**
 * Checks if string starts with a consonant.
 * Default: Alphabetic.
 */
fun String.startsWithConsonant(checkAlphabetic: Boolean=true): Boolean =
        if (checkAlphabetic)
            this.startsWithAlphaConsonant()
        else
            this.startsWithAcroConsonant()

/**
 * Checks if string of length 1 is a vowel
 */
fun String.isVowel(): Boolean = if (this.length == 1) this[0].toLowerCase() in ALPHABETIC_VOWELS else false

/**
 * Checks if string only contains letters
 */
fun String.isAlpha(): Boolean = this.all { character -> character.isLetter() }

/**
 * Checks if string is able to be converted to a number as is.
 */
fun String.isNumeric(): Boolean = this.toBigDecimalOrNull() is BigDecimal

/**
 * Checks if string only contains symbols
 */
fun String.isSymbolic(): Boolean = this.all { character -> !character.isLetterOrDigit() }

/**
 * Checks if string only contains letters and\or numbers
 */
fun String.isAlphaNumeric(): Boolean = this.all { character -> character.isLetterOrDigit() }

/**
 * Checks if string only contains letters and\or symbols
 */
fun String.isAlphaSymbolic(): Boolean = this.all { character -> character.isLetter() or !character.isLetterOrDigit()}

/**
 * Checks if at least 1 letter in string is upper case and at least 1 letter in string is lower case
 */
fun String.notFullCaps(): Boolean = this.any { character -> character.isUpperCase() } and this.any { character -> character.isLowerCase() }

/**
 * Checks if first character of string is capitalized and all letters in string are NOT capitalized, otherwise just use [String.isUpperCase]
 */
fun String.isCapitalized(): Boolean = this[0].isUpperCase() and this.notFullCaps()

/**
 * Checks if all letters in strings are upper case
 */
fun String.isUpperCase(): Boolean =
    this.toCharArray()
    .filter { character -> character.isLetter() }
    .all { character -> character.isUpperCase() }

/**
 * Checks if all letters in string are lower case
 */
fun String.isLowerCase(): Boolean =
    this.toCharArray()
    .filter { character -> character.isLetter() }
    .all { character -> character.isLowerCase() }


/**
 * Gets the correct indefinite article for string.
 * This function assumes the word is singular, as detecting a plural would be quite a hassle with words that end with e and s.
 * Singular Indefinite article is "An" or "A", ex: A cookie, An animal, etc.
 * Plural indefinite article is "Some", ex: Some cookies, Some animals, etc.
 * Single Definite article is "The", ex: The cookie, The animal, etc.
 * Plural Definite article is "Those", ex: Those cookies, those animals, etc.
 */
fun String.getArticle(): String =
    if (this.isUpperCase()) {
        when (this[0]) {
            in ACRONYMIC_VOWELS -> "an"
            else -> "a"
        }
    } else {
        when (startsWithAlphaVowel()) {
            true -> "an"
            false -> "a"
        }
    }
