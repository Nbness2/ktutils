package TextUtil

/**
 * @author: nbness2
 */

/**
 * The [String.rpad] function pads the string from the right side using the given [padChar], which must be only 1 character long.
 */
fun String.rpad(padLength: Int, padChar: String): String {
    if (this.length >= padLength)
        return this
    if (padChar.length != 1)
        throw IndexOutOfBoundsException("padChar must be 1 character long, not ${padChar.length}")
    val returnString = StringBuilder(this)
    for (char in this.length until padLength)
        returnString.append(padChar)
    return returnString.toString()
}

fun String.rpad(padLength: Int, padChar: Char): String {
    if (this.length >= padLength)
        return this
    val returnString = StringBuilder(this)
    for (char in this.length until padLength)
        returnString.append(padChar)
    return returnString.toString()
}

/**
 * This does the same thing as [String.lpad] but from the left rather than right.
 */
fun String.lpad(padLength: Int, padChar: String): String {
    if (this.length >= padLength)
        return this
    if (padChar.length != 1)
        throw IndexOutOfBoundsException("padChar must be 1 character long, not ${padChar.length}")
    val returnString = StringBuilder()
    for (char in this.length until padLength)
        returnString.append(padChar)
    return returnString.append(this).toString()
}

fun String.lpad(padLength: Int, padChar: Char): String {
    if (this.length >= padLength)
        return this
    val returnString = StringBuilder()
    for (char in this.length until padLength)
        returnString.append(padChar)
    return returnString.append(this).toString()
}


fun main() {
    val a = "0123"
    println(a.rpad(10, 'z'))
    println(a.rpad(10, "z"))
    println(a.lpad(10, 'z'))
    println(a.lpad(10, "z"))
}