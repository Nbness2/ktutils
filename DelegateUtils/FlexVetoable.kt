package DelegateUtils

import kotlin.properties.ReadWriteProperty
import kotlin.reflect.KProperty

/**
 * @Author: nbness2
 */

/**
 * Implements the core logic of a property delegate. Can callback at 4 different points in the assignment process.
 */
abstract class FlexVetoable<T>(initialValue: T) : ReadWriteProperty<Any?, T> {
    private var value = initialValue

    /**
     *  The callback which is called before anything is attempted. This is here to reduce clutter in passCondition functions
     *  and provide a bit more clarity in to what is going on, otherwise has no additional function.
     *  Most likely use case is logging.
     */
    protected open fun preInvoke(property: KProperty<*>, oldValue: T, newValue: T): Unit {}

    /**
     *  The callback which is called if the change to the property value fails.
     *  The value of the property isn't changed in this instance, and this callback is invoked.
     */

    protected open fun onFail(property: KProperty<*>, oldValue: T, newValue: T): Unit {}

    /**
     *  The callback which is called before a change to the property value is attempted.
     *  The value of the property hasn't been changed yet, when this callback is invoked.
     *  If the callback returns `true` the value of the property is being set to the new value,
     *  and if the callback returns `false` the new value is discarded and the property remains its old value.
     */
    protected open fun passCondition(oldValue: T, newValue: T): Boolean = true

    /**
     * The callback which is called after the change of the property is made. The value of the property
     * has already been changed when this callback is invoked.
     */
    protected open fun afterChange(property: KProperty<*>, oldValue: T, newValue: T): Unit {}

    /**
     * This is what is called when you access just the reference that this delegate is assigned to.
     * Example:
     *      var a: Int by Delegate(10)
     *      a = 15
     *      println(a) // getValue is called here
     */
    public override fun getValue(thisRef: Any?, property: KProperty<*>): T = this.value

    /**
     * This is what is called when you access just the reference that this delegate is assigned to.
     * Example:
     *      var a: Int by Delegate(10)
     *      a = 15 // setValue is called here
     *      println(a)
     */
    public override fun setValue(thisRef: Any?, property: KProperty<*>, value: T) {
        val oldValue = this.value
        preInvoke(property, oldValue, value)
        if (oldValue == value) {
            afterChange(property, oldValue, value)
            return
        }
        if (!passCondition(oldValue, value)) {
            onFail(property, oldValue, value)
            return
        }
        this.value = value
        afterChange(property, oldValue, value)
    }
}


class FlexVetoBuilder<T> {
    var initialValue: T? = null
    var preInvoke: (KProperty<*>, oldValue: T, newValue: T) -> Unit = {_, _, _ -> Unit}
    var passCondition: (oldValue: T, newValue: T) -> Boolean = {_, _ -> true}
    var onFail: (KProperty<*>, oldValue: T, newValue: T) -> Unit = {_, _, _ -> Unit}
    var onSuccess: (KProperty<*>, oldValue: T, newValue: T) -> Unit = {_, _, _ -> Unit}
    fun buildFlexVeto(): ReadWriteProperty<Any?, T> = createFlexVetoable(initialValue ?: throw Exception("Cannot have null initialValue in FlexVetoBuilder"), preInvoke, passCondition, onFail, onSuccess)
}


inline fun <T> createFlexVetoable(
    initialValue: T,
    crossinline preInvoke: (property: KProperty<*>, oldValue: T, newValue: T) -> Unit = { _, _, _ -> Unit},
    crossinline passCondition: (oldValue: T, newValue: T) -> Boolean = {_, _ -> true},
    crossinline onFail: (property: KProperty<*>, oldValue: T, newValue: T) -> Unit = {_, _, _ -> Unit},
    crossinline onSuccess: (property: KProperty<*>, oldValue: T, newValue: T) -> Unit = { _, _, _ -> Unit}
): ReadWriteProperty<Any?, T> {
    if (!passCondition(initialValue, initialValue)) {
        throw Exception("Initial value $initialValue did not pass in FlexVetoable creation")
    }
    return object : FlexVetoable<T>(initialValue) {
        override fun passCondition(oldValue: T, newValue: T): Boolean =
            passCondition(oldValue, newValue)

        override fun afterChange(property: KProperty<*>, oldValue: T, newValue: T) =
            onSuccess(property, oldValue, newValue)

        override fun preInvoke(property: KProperty<*>, oldValue: T, newValue: T) =
            preInvoke(property, oldValue, newValue)

        override fun onFail(property: KProperty<*>, oldValue: T, newValue: T) =
            onFail(property, oldValue, newValue)
    }
}

inline fun <T> buildFlexVetoable(builder: FlexVetoBuilder<T>.() -> Unit): ReadWriteProperty<Any?, T> = FlexVetoBuilder<T>().apply(builder).buildFlexVeto()

// testDelegate holds an Int and can be treated as such anywhere that it is used.
var testDelegate: Int by buildFlexVetoable {
    initialValue = 25 // initialValue must be same type as the type you are delegating, in this case Int
    preInvoke = { property, oldValue, newValue ->
        // this is where you might log something, otherwise this pretty much has no use. nothing has been changed at this point.
        println("preInvoke called, this is where you might log something related to ($property), $oldValue and $newValue")
    }
    passCondition = { oldValue, newValue ->
        // if this condition fails, onFail will be called, otherwise onSuccess will be called. at this point, nothing has been changed
        println("passCondition called, this is where you check $newValue against your given condition.")
        newValue in 25 .. 25_000
    }
    onFail = { property, oldValue, newValue ->
        // this is your last change to use newValue before it is gone
        println("onFail called, ($property) has not been changed from $oldValue.")
    }
    onSuccess = { property, oldValue, newValue ->
        // this is your last chance to use oldValue before it is gone
        println("onSuccess called, ($property) has now been set from $oldValue to $newValue")
    }
}

fun main() {
    testDelegate = 24_999
    println(testDelegate) // should print 24999
    testDelegate += 1 // changes from 24999 to 25000
    println(testDelegate) // should print 25000
    testDelegate++ // attempts to change from 25000 to 25001 but fails the passCondition, so stays at 25000
    println(testDelegate) // should print 25000
}